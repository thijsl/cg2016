function GeometricObject() {
    this.material = null;
    this.transformation = null;
    this.shadows = true;
    this.kEpsilon = 0.0001;
};
GeometricObject.prototype = {
    hit: function (ray, tmin, sr) {
    },
    set_material: function (material) {
        this.material = material;
    },
    set_transformation: function (t) {
        this.transformation = t;
    },
    pdf: function (sr) {

    },
    sample: function () {

    },
    getCenter: function () {
        var bBox = this.computeBoundingBox();
        return bBox.min.add(bBox.max).scale(0.5);
    },
    set_shadows: function (visibility) {
        this.shadows = visibility;
    }
};

function Triangle(a, b, c, na, nb, nc, ta, tb, tc) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.na = new Normal(na.x, na.y, na.z);
    this.nb = new Normal(nb.x, nb.y, nb.z);
    this.nc = new Normal(nc.x, nc.y, nc.z);
    this.ta = ta;
    this.tb = tb;
    this.tc = tc;
    this.t0 = 0;
    this.t1 = Number.MAX_VALUE;
}
Triangle.prototype = new GeometricObject();
Triangle.prototype.hit = function (ray, tmin, sr) {
    var transformed = this.transformation.transformInverse(ray);
    var a = this.a.x - this.b.x,
        b = this.a.y - this.b.y,
        c = this.a.z - this.b.z,
        d = this.a.x - this.c.x,
        e = this.a.y - this.c.y,
        f = this.a.z - this.c.z,
        g = transformed.direction.x,
        h = transformed.direction.y,
        i = transformed.direction.z,
        j = this.a.x - transformed.origin.x,
        k = this.a.y - transformed.origin.y,
        l = this.a.z - transformed.origin.z,
        M = a * (e * i - h * f) + b * (g * f - d * i) + c * (d * h - e * g),
        beta = (j * (e * i - h * f) + k * (g * f - d * i) + l * (d * h - e * g)) / M,
        gamma = (i * ( a * k - j * b) + h * (j * c - a * l) + g * (b * l - k * c)) / M,
        t = (-1) * ((f * (a * k - j * b) + e * (j * c - a * l) + d * (b * l - k * c))) / M,
        sr = new ShadeRec(sr),
        hr = new HitRecord(false, sr, t);
    if (t < this.kEpsilon)
        return hr;
    if (t < this.t0 || t > this.t1)
        return hr;
    if (gamma < 0 || gamma > 1)
        return hr;
    if (beta < 0 || beta > 1 - gamma)
        return hr;
    // console.log("this ne hit");
    this.beta = beta;
    this.gamma = gamma;
    sr.hit_an_object = true;
    sr.t = t;
    if (this.transformation) {
        sr.normal = this.transformation.transformInverse(this.getWeightedNormal());
        // sr.normal = this.getWeightedNormal(); // fix
    } else {
        sr.normal = this.getWeightedNormal();
    }
    sr.local_hit_point = transformed.origin.add(transformed.direction.scale(t));
    sr.material = this.material;
    // if (t < Number.MAX_VALUE) {
    //     sr.hit_point = transformed.direction.scale(t).add(transformed.origin);
    // }
    hr = new HitRecord(true, sr, t);
    return hr;
};
Triangle.prototype.getWeightedNormal = function () {
    var alpha = 1 - this.beta - this.gamma,
        normalVector = this.na.scale(alpha).add(this.nb.scale(this.beta)).add(this.nc.scale(this.gamma));
    return normalVector.toNormal();
};
Triangle.prototype.shadow_hit = function (ray, tmin, sr) {
    return this.hit(ray, tmin, sr);
};
Triangle.prototype.computeBoundingBox = function () {
    var x_min = Math.min(this.a.x, Math.min(this.b.x, this.c.x)),
        y_min = Math.min(this.a.y, Math.min(this.b.y, this.c.y)),
        z_min = Math.min(this.a.z, Math.min(this.b.z, this.c.z)),
        x_max = Math.max(this.a.x, Math.max(this.b.x, this.c.x)),
        y_max = Math.max(this.a.y, Math.max(this.b.y, this.c.y)),
        z_max = Math.max(this.a.z, Math.max(this.b.z, this.c.z));
    return new BBox(new Point(x_min, y_min, z_min), new Point(x_max, y_max, z_max));
};
Triangle.prototype.getCenter = function () {
    var bBox = this.computeBoundingBox();
    return bBox.min.add(bBox.max).scale(0.5);
};
Triangle.index = function(
    vertices, uvs, normals, tangents, bitangents,
    out_indices, out_tangents, out_bitangents, out_vertices, out_uvs, out_normals) {
    for (var i = 0; i < vertices.length; i++) {
        var similarVertex = Triangle.getSimilarVertexIndex(
            this[vertices[i]], this["t"+vertices[i]], this["n"+[i]],
            out_vertices, out_uvs, out_normals
        );
        var index = similarVertex.index;
        if (similarVertex.found) {
            out_indices.push(similarVertex.index);
            out_tangents[index] += tangents[i];
            out_bitangents[index] += bitangents[i];
        } else {
            out_vertices.push(this[vertices[i]]);
            out_uvs.push(this[vertices[i]]);
            out_normals.push(this[vertices[i]]);
            out_indices.push(out_vertices.length-1);
            out_tangents.push(tangents[i]);
            out_bitangents.push(bitangents[i]);
        }
    }
    return {
        out_indices : out_indices,
        out_tangents : out_tangents,
        out_bitangents : out_bitangents,
        out_vertices : out_vertices,
        out_uvs : out_uvs,
        out_normals : out_normals
    }
};
Triangle.is_near = function(v1, v2) {
    return (Math.abs(v1-v2) < 0.01);
};
Triangle.getSimilarVertexIndex = function(vertex, uv, normal, out_vertices, out_uvs, out_normals) {
    for (var i = 0; i < out_vertices.length; i++) {
        if (
            Triangle.is_near(vertex.x, out_vertices[i].x) &&
            Triangle.is_near(vertex.y, out_vertices[i].y) &&
            Triangle.is_near(vertex.z, out_vertices[i].z) &&
            Triangle.is_near(uv.x, out_uvs[i].x) &&
            Triangle.is_near(uv.y, out_uvs[i].y) &&
            Triangle.is_near(normal.x, out_normals[i].x) &&
            Triangle.is_near(normal.y, out_normals[i].y) &&
            Triangle.is_near(normal.z, out_normals[i].z)
        ) {
            return {
                index: i,
                found: true
            }
        }
    }
    return {
        index: 0,
        found: false
    }
};

function Plane(point, normal) {
    this.point = point;
    this.normal = new Normal(normal.x, normal.y, normal.z);
    this.normal = this.normal.normalize();
}
Plane.prototype = new GeometricObject();

Plane.prototype.hit = function (ray, tmin, sr) {
    sr = new ShadeRec(sr);
    var t = ((this.point.subtract(ray.origin)).dotVector(this.normal)) / (ray.direction.dot(this.normal)),
        hitRecord;
    sr.t = t;
    if (t > this.kEpsilon) {
        sr.normal = this.normal;
        sr.local_hit_point = ray.origin.add(ray.direction.scale(t));
        sr.material = this.material;
        sr.hit_an_object = true;
        hitRecord = new HitRecord(true, sr, t);
        return hitRecord;
    } else {
        sr.normal = new Vector(1, 1, 1);
        hitRecord = new HitRecord(false, sr, t);
        return hitRecord;
    }
};
Plane.prototype.shadow_hit = function (ray, tmin, sr) {
    var t = ((this.point.subtract(ray.origin)).dot(this.normal)) / (ray.direction.dot(this.normal)),
        hitRecord;
    if (t > this.kEpsilon) {
        hitRecord = new HitRecord(true, sr, t);
        return hitRecord;
    } else {
        hitRecord = new HitRecord(false, sr, t);
        return hitRecord;
    }
};
Plane.prototype.computeBoundingBox = function () {
    return new BBox(new Point(-Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE), new Point(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE));
};
Plane.prototype.getCenter = function () {
    var bBox = this.computeBoundingBox();
    return bBox.min.add(bBox.max).scale(0.5);
};

function Sphere(center, radius) {
    this.center = center;
    this.radius = radius;
    this.inv_surface_area = 1 / (4 * Math.PI * radius * radius);
};
Sphere.prototype = new GeometricObject();
Sphere.prototype.hit = function (ray, tmin, sr) {
    sr = new ShadeRec(sr);
    var t = 0,
        hitRecord = new HitRecord(false, sr, t),
        transformed = this.transformation.transformInverse(ray);
    var origin = transformed.origin.subtract(this.center),
        a = transformed.direction.dot(transformed.direction),
        b = 2.0 * (origin.dot(transformed.direction)),
        c = origin.dot(origin) - this.radius * this.radius;
    sr.material = this.material;
    hitRecord.sr = sr;
    var disc = b * b - (4 * a * c);
    if (disc < 0.0) {
        return hitRecord;
    } else {
        var e = Math.sqrt(disc),
            denom = 2.0 * a;
        t = (-b - e) / denom;
        sr.hit_an_object = true;
        if (t > this.kEpsilon) {
            sr.t = t;
            sr.normal = (origin.add(transformed.direction.scale(t))).divide(this.radius);
            sr.local_hit_point = transformed.origin.add(transformed.direction.scale(t));
            hitRecord = new HitRecord(true, sr, t);
            return hitRecord;
        }
        t = (-b + e) / denom;
        if (t > this.kEpsilon) {
            sr.t = t;
            sr.normal = (origin.add(transformed.direction.scale(t))).divide(this.radius);
            sr.local_hit_point = transformed.origin.add(transformed.direction.scale(t));
            hitRecord = new HitRecord(true, sr, t);
            return hitRecord;
        }
    }
    return hitRecord;
};
Sphere.prototype.shadow_hit = function (ray, tmin, sr) {
    return this.hit(ray, tmin, sr);
}
Sphere.prototype.getCenter = function () {
    return this.center;
};
Sphere.prototype.get_normal = function (p) {
    var n = this.center.subtract(p).normalize();
    return new Normal(n.x, n.y, n.z);
}
Sphere.prototype.computeBoundingBox = function () {
    var v1 = new Vector(-this.radius, -this.radius, -this.radius),
        v2 = new Vector(this.radius, this.radius, this.radius);
    return new BBox(this.center.add(v1), this.center.add(v2));
};
Sphere.prototype.pdf = function (sr) {
    return this.inv_surface_area;
};
Sphere.prototype.sample = function () {
    return this.sampler.sampleSphere().scale(this.radius)
        .add(new Vector(this.center, this.center, this.center))
}

function Cylinder(start, end, radius) {
    this.start = start;
    this.radius = radius;
    this.end = end;
    this.normal = new Vector(0, 0, 0);
    this.inv_radius = 1 / radius;
};
Cylinder.prototype = new GeometricObject();
Cylinder.prototype.hit = function (ray2, tmin, sr) {
    sr = new ShadeRec(sr);
    var ray = this.transformation.transformInverse(ray2),
        a = (ray.direction.x) * (ray.direction.x) + (ray.direction.y) * (ray.direction.y),
        b = 2 * (ray.origin.x) * (ray.direction.x) + 2 * (ray.origin.y) * (ray.direction.y),
        c = (ray.origin.x) * (ray.origin.x) + (ray.origin.y) * (ray.origin.y) - 1,
        d = b * b - 4.0 * a * c;
    sr.material = this.material;
    if (d < 0) {
        return new HitRecord(false, sr, tmin);
    }
    var dr = Math.sqrt(d),
        q = b < 0 ? -0.5 * (b - dr) : -0.5 * (b + dr),
        t1 = q / a,
        t2 = c / q,
        z1 = ray.origin.add(ray.direction.scale(t1)).z,
        z2 = ray.origin.add(ray.direction.scale(t2)).z,
        t3 = -1;
    if ((this.start < z1 && z1 < this.end) || (this.start < z2 && z2 < this.end)) {
        t3 = (this.start - ray.origin.z) / ray.direction.z;
    }
    sr.hit_an_object = (t3 >= 0);
    sr.t = t3;
    sr.local_hit_point = ray.origin.add(ray.direction.scale(sr.t));
    sr.normal = (ray.origin.add(ray.direction.scale(t3))).divide(this.radius);
    console.log(sr);
    return new HitRecord((t3 >= 0), sr, t3);
};

Cylinder.prototype.shadow_hit = function (ray, tmin, sr) {
    return this.hit(ray, tmin, sr);
};
// Cylinder.prototype.get_normal = function()
Cylinder.prototype.computeBoundingBox = function () {
    var min = new Point(-this.radius, this.start, -this.radius),
        max = new Point(this.radius, this.end, this.radius);
    return new BBox(min, max);
};

function OpenCylinder(start, end, radius) {
    this.start = start;
    this.radius = radius;
    this.end = end;
    this.normal = new Vector(0, 0, 0);
    this.inv_radius = 1 / radius;
};
OpenCylinder.prototype = new GeometricObject();
OpenCylinder.prototype.hit = function (ray2, tmin, sr) {
    sr = new ShadeRec(sr);
    var ray = this.transformation.transformInverse(ray2),
        a = (ray.direction.x) * (ray.direction.x) + (ray.direction.z) * (ray.direction.z),
        b = 2 * (ray.origin.x) * (ray.direction.x + ray.origin.z * ray.direction.z),
        c = (ray.origin.x) * (ray.origin.x) + (ray.origin.z) * (ray.origin.z) - (this.radius * this.radius),
        d = b * b - 4.0 * a * c;
    sr.material = this.material;
    if (d < 0) {
        return new HitRecord(false, sr, tmin);
    }
    var dr = Math.sqrt(d),
        denom = 2 * a,
        t = (-b - dr) / denom,
        process = function (ray, sr, t, start, end, inv_radius) {
            var yhit = ray.origin.y + t * ray.direction.y;
            if (yhit > start && yhit < end) {
                sr.t = t;
                sr.normal = new Normal((ray.origin.x + t * ray.direction.x) * inv_radius,
                    0, (ray.origin.z + t * ray.direction.z) * inv_radius);
                if (ray.direction.negate().dot(sr.normal.x) < 0) {
                    sr.normal = sr.normal.negate();
                }
                sr.local_hit_point = ray.origin.add(ray.direction.scale(sr.t));
                sr.hit_an_object = true;
                return new HitRecord(true, sr, t);
            }
        };
    if (t > this.kEpsilon) {
        var processResult = process(ray, sr, t, this.start, this.end, this.inv_radius);
        if (processResult) {
            return processResult;
        }
    }
    t = (-b + dr) / denom;
    if (t > this.kEpsilon) {
        var processResult = process(ray, sr, t, this.start, this.end, this.inv_radius);
        if (processResult) {
            return processResult;
        }
    }
    return new HitRecord(false, sr, t);
};

OpenCylinder.prototype.shadow_hit = function (ray, tmin, sr) {
    return this.hit(ray, tmin, sr);
};
// Cylinder.prototype.get_normal = function()
OpenCylinder.prototype.computeBoundingBox = function () {
    var min = new Point(-this.radius, this.start, -this.radius),
        max = new Point(this.radius, this.end, this.radius);
    return new BBox(min, max);
};

function Torus(a, b) {
    this.a = a;
    this.b = b;
    this.bbox = new BBox(new Point(-this.a - this.b, -this.b, -this.a - this.b),
        new Point(this.a + this.b, this.b, this.a + this.b));

};
Torus.prototype = new GeometricObject();
Torus.prototype.hit = function (ray, tmin, sr) {
    sr = new ShadeRec(sr);
    sr.material = this.material;
    var hitRecord = new HitRecord(false, sr, tmin);
    if (!this.bbox.hit(ray).hit) {
        return hitRecord;
    }
    var x1 = ray.origin.x, y1 = ray.origin.y, z1 = ray.origin.z,
        d1 = ray.direction.x, d2 = ray.direction.y, d3 = ray.direction.z,
        coeffs = [], 	// coefficient array
        roots,		// solution array
        sum_d_sqrd = d1 * d1 + d2 * d2 + d3 * d3,
        e = x1 * x1 + y1 * y1 + z1 * z1 - this.a * this.a - this.b * this.b,
        f = x1 * d1 + y1 * d2 + z1 * d3,
        four_a_sqrd = 4 * this.a * this.a;
    coeffs[0] = e * e - four_a_sqrd * (this.b * this.b - y1 * y1); 		// constan term
    coeffs[1] = 4.0 * f * e + 2.0 * four_a_sqrd * y1 * d2;
    coeffs[2] = 2.0 * sum_d_sqrd * e + 4.0 * f * f + four_a_sqrd * d2 * d2;
    coeffs[3] = 4.0 * sum_d_sqrd * f;
    coeffs[4] = sum_d_sqrd * sum_d_sqrd; 	// coefficient of t^4

    var quarticSol = Math.solveQuartic(coeffs),
        num_real_roots = quarticSol.n,
        intersected = false,
        t = Number.MAX_VALUE;
    roots = quarticSol.roots;

    if (num_real_roots === 0) {// ray misses the torus
        return hitRecord
    }

    for (var j = 0; j < num_real_roots; j++) {
        if (roots[j] > this.kEpsilon) {
            intersected = true;
            if (roots[j] < t) {
                t = roots[j];
            }
        }
    }
    if (!intersected) {
        return hitRecord;
    }
    hitRecord.t = t;
    sr.t = t;
    sr.hit_an_object = true;
    sr.local_hit_point = ray.origin.add(ray.direction.scale(t));
    return new HitRecord(true, sr, t);
};
Torus.prototype.computeBoundingBox = function () {
    return this.bbox;
}

function Beam(min, max) {
    this.min = min;
    this.max = max;
    this.normal = new Vector(0, 0, 1);
};
Beam.prototype = new GeometricObject();
Beam.prototype.hit = function (ray, tmin, sr) {
    // sr = new ShadeRec(sr);
    // sr.material = this.material;
    // var t_xMin, t_xMax, t_yMin, t_yMax, t_zMin, t_zMax;
    // var ax = 1 / ray.direction.x,
    //     ay = 1 / ray.direction.y,
    //     az = 1 / ray.direction.z;
    // if (ax >= 0) {
    //     t_xMin = ax * (this.min.x - ray.origin.x);
    //     t_xMax = ax * (this.max.x - ray.origin.x);
    // } else {
    //     t_xMin = ax * (this.max.x - ray.origin.x);
    //     t_xMax = ax * (this.min.x - ray.origin.x);
    // }
    // if (ay >= 0) {
    //     t_yMin = ay * (this.min.y - ray.origin.y);
    //     t_yMax = ay * (this.max.y - ray.origin.y);
    // } else {
    //     t_yMin = ay * (this.max.y - ray.origin.y);
    //     t_yMax = ay * (this.min.y - ray.origin.y);
    // }
    // if (az >= 0) {
    //     t_zMin = az * (this.min.z - ray.origin.z);
    //     t_zMax = az * (this.max.z - ray.origin.z);
    // } else {
    //     t_zMin = az * (this.max.z - ray.origin.z);
    //     t_zMax = az * (this.min.z - ray.origin.z);
    // }
    // if ((t_xMin > t_yMax || t_xMin > t_zMax)
    //     || (t_yMin > t_xMax || t_yMin > t_zMax)
    //     || (t_zMin > t_xMax || t_zMin > t_yMax)) {
    //     sr.hit_an_object = false;
    //     sr.normal = this.normal;
    //     return new HitRecord(false, sr, t_xMax);
    // } else {
    //     sr.hit_an_object = true;
    //     sr.normal = this.normal;
    //     return new HitRecord(true, sr, t_xMax);
    // }
    ray = this.transformation.transformInverse(ray);
    sr.ray = ray;
    sr.material = this.material;
    var origin = ray.origin,
        direction = ray.direction,
        tx_min, ty_min, tz_min,
        tx_max, ty_max, tz_max,
        a = 1 / direction.x,
        b = 1 / direction.y,
        c = 1 / direction.z,
        t0, t1,
        face_in, face_out,
        hitRecord = new HitRecord(false);
    if (a >= 0) {
        tx_min = (this.min.x - origin.x) * a;
        tx_max = (this.max.x - origin.x) * a;
    } else {
        tx_min = (this.max.x - origin.x) * a;
        tx_max = (this.min.x - origin.x) * a;
    }
    if (b >= 0) {
        ty_min = (this.min.y - origin.y) * b;
        ty_max = (this.max.y - origin.y) * b;
    } else {
        ty_min = (this.max.y - origin.y) * b;
        ty_max = (this.min.y - origin.y) * b;
    }
    if (c >= 0) {
        tz_min = (this.min.z - origin.z) * c;
        tz_max = (this.max.z - origin.z) * c;
    } else {
        tz_min = (this.max.z - origin.z) * c;
        tz_max = (this.min.z - origin.z) * c;
    }

    // largest entering t value
    if (tx_min > ty_min) {
        t0 = tx_min;
        face_in = (a >= 0) ? 0 : 3;
    } else {
        t0 = ty_min;
        face_in = (a >= 0) ? 1 : 4;
    }
    if (tz_min > t0) {
        t0 = tz_min;
        face_in = (a >= 0) ? 2 : 5;
    }

    // smallest exiting t value
    if (tx_max < ty_max) {
        t1 = tx_max;
        face_out = (a >= 0) ? 3 : 0;
    } else {
        t1 = ty_max;
        face_out = (a >= 0) ? 4 : 1;
    }
    if (tz_max < t1) {
        t1 = tz_max;
        face_out = (a >= 0) ? 5 : 2;
    }
    if (t0 < t1 && t1 > this.kEpsilon) {
        if (t0 > this.kEpsilon) {
            sr.t = t0;
            sr.normal = this.get_normal(face_in);
        } else {
            sr.t = t1;
            sr.normal = this.get_normal(face_out);
        }
        sr.local_hit_point = ray.origin.add(ray.direction.scale(sr.t));
        sr.hit_an_object = true;
    } else {
        sr.hit_an_object = false;
    }
    return new HitRecord(sr.hit_an_object, sr, sr.t);
};
Beam.prototype.shadow_hit = function (ray, tmin, sr) {
    return this.hit(ray, tmin, sr);
};
Beam.prototype.computeBoundingBox = function() {
    return new BBox(this.min, this.max);
};
Beam.prototype.get_normal = function(i) {
    switch (i) {
        case 0:
            return (new Normal(-1, 0, 0));	// -x face
        case 1:
            return (new Normal(0, -1, 0));	// -y face
        case 2:
            return (new Normal(0, 0, -1));	// -z face
        case 3:
            return (new Normal(1, 0, 0));	// +x face
        case 4:
            return (new Normal(0, 1, 0));	// +y face
        case 5:
            return (new Normal(0, 0, 1));	// +z face
        default:
            return new Normal(0, 0, 0);
    }
};

function Rectangle(p0, a, b) {
    this.p0 = p0;
    this.a = a;
    this.b = b;
    this.a_squared_length = a.squared_length();
    this.b_squared_length = b.squared_length();
    this.area = Math.sqrt(this.a_squared_length) * Math.sqrt(this.b_squared_length);
    this.inv_area = 1 / this.area;
    this.normal = this.a.cross(this.b).normalize();
}
Rectangle.prototype = new GeometricObject();
Rectangle.prototype.set_sampler = function (sampler) {
    this.sampler = sampler;
};
Rectangle.prototype.sample = function () {
    var sample_point = this.sampler.sample_unit_square();
    return (this.p0.add(this.a.scale(sample_point.x).add(this.b.scale(sample_point.y))));
};
Rectangle.prototype.hit = function (ray, tmin, sr) {
    var ray = this.transformation.transformInverse(ray);
    sr.material = this.material;
    //plane intersection parameter
    var t = this.p0.subtract(ray.origin).dot(this.normal) / ray.direction.dot(this.normal),
        hitRecord = new HitRecord(false, sr, t);
    if (t <= this.kEpsilon) {
        return hitRecord;
    }

    var p = ray.origin.add(ray.direction.scale(t)), //hit point
        d = p.subtract(this.p0), //from point on plane to hit point
        ddota = d.dot(this.a);//project onto a

    //we're outside of the a direction
    if (ddota < 0 || ddota > this.a_squared_length) {
        return hitRecord;
    }

    //project onto b
    var ddotb = d.dot(this.b);
    //we're outside of the b direction
    if (ddotb < 0 || ddotb > this.b_squared_length) {
        return hitRecord;
    }
    //if we're here we hit and set shaderec up
    sr.t = t;
    sr.normal = this.normal;
    sr.local_hit_point = p;
    sr.hit_an_object = true;
    hitRecord.hit = true;
    hitRecord.t = t;
    return hitRecord;
};
Rectangle.prototype.shadow_hit = function (ray, tmin, sr) {
    return this.hit(ray, tmin, sr);
};
Rectangle.prototype.computeBoundingBox = function () {
    var delta = 0.0001,
        min = new Point(
            Math.min(this.p0.x, this.p0.x + this.a.x + this.b.x) - delta,
            Math.min(this.p0.y, this.p0.y + this.a.y + this.b.y) - delta,
            Math.min(this.p0.z, this.p0.z + this.a.z + this.b.z) - delta
        ),
        max = new Point(
            Math.max(this.p0.x, this.p0.x + this.a.x + this.b.x) + delta,
            Math.max(this.p0.y, this.p0.y + this.a.y + this.b.y) + delta,
            Math.max(this.p0.z, this.p0.z + this.a.z + this.b.z) + delta
        );
    return new BBox(min, max)

};
Rectangle.prototype.get_normal = function (p) {
    return this.normal;
};
Rectangle.prototype.pdf = function (sr) {
    return this.inv_area;
};

var kEpsilon = 0.00001;
function BBox(min, max) {
    this.min = min;
    this.max = max;
}
BBox.prototype = new GeometricObject();
BBox.prototype.hit = function (ray, tmin, sr) {
    ray = this.transformation.transformInverse(ray);
    sr.ray = ray;
    var origin = new Point(ray.origin.x, ray.origin.y, ray.origin.z),
        direction = new Point(ray.direction.x, ray.direction.y, ray.direction.z),
        tx_min, ty_min, tz_min,
        tx_max, ty_max, tz_max,
        a = 1 / direction.x,
        b = 1 / direction.y,
        c = 1 / direction.z,
        t0, t1,
        hitRecord = new HitRecord(false);
    if (a >= 0) {
        tx_min = (this.min.x - origin.x) * a;
        tx_max = (this.max.x - origin.x) * a;
    } else {
        tx_min = (this.max.x - origin.x) * a;
        tx_max = (this.min.x - origin.x) * a;
    }
    if (b >= 0) {
        ty_min = (this.min.y - origin.y) * b;
        ty_max = (this.max.y - origin.y) * b;
    } else {
        ty_min = (this.max.y - origin.y) * b;
        ty_max = (this.min.y - origin.y) * b;
    }
    if (c >= 0) {
        tz_min = (this.min.z - origin.z) * c;
        tz_max = (this.max.z - origin.z) * c;
    } else {
        tz_min = (this.max.z - origin.z) * c;
        tz_max = (this.min.z - origin.z) * c;
    }

    // largest entering t value
    if (tx_min > ty_min) {
        t0 = tx_min;
    } else {
        t0 = ty_min;
    }
    if (tz_min > t0) {
        t0 = tz_min;
    }

    // smallest exiting t value
    if (tx_max < ty_max) {
        t1 = tx_max;
    } else {
        t1 = ty_max;
    }
    if (tz_max < t1) {
        t1 = tz_max;
    }
    return new HitRecord((t0 < t1 && t1 > kEpsilon), sr, t1);
};

BBox.prototype.computeBoundingBox = function (geometrics) {
    var bbox = this,
        x_min = bbox.min.x,
        y_min = bbox.min.y,
        z_min = bbox.min.z,
        x_max = bbox.max.x,
        y_max = bbox.max.y,
        z_max = bbox.max.z,
        i = 0,
        bboxTmp;

    for (i; i < geometrics.length; i++) {
        bboxTmp = geometrics[i].computeBoundingBox(geometrics);
        x_min = Math.min(x_min, bboxTmp.min.x);
        y_min = Math.min(y_min, bboxTmp.min.y);
        z_min = Math.min(z_min, bboxTmp.min.z);
        x_max = Math.max(x_max, bboxTmp.max.x);
        y_max = Math.max(y_max, bboxTmp.max.y);
        z_max = Math.max(z_max, bboxTmp.max.z);
    }
    return new BBox(new Point(x_min, y_min, z_min), new Point(x_max, y_max, z_max));
};
BBox.prototype.combine = function (other) {
    var minx = Math.min(this.min.x, other.min.x),
        miny = Math.min(this.min.y, other.min.y),
        minz = Math.min(this.min.z, other.min.z),
        maxx = Math.max(this.max.x, other.max.x),
        maxy = Math.max(this.max.y, other.max.y),
        maxz = Math.max(this.max.z, other.max.z);
    return new BBox(new Point(minx, miny, minz), new Point(maxx, maxy, maxz));
};

function BvhNode(geometrics, axis, transformation) {
    this.axis = axis;
    var N = geometrics.length;
    if (N === 1) {
        this.left = geometrics[0];
        this.right = null;
        this.bBox = geometrics[0].computeBoundingBox(geometrics);
    } else {
        geometrics.sort(this.axisComparator);
        this.left = new BvhNode(geometrics.slice(0, geometrics.length / 2), (axis + 1) % 3, transformation);
        this.right = new BvhNode(geometrics.slice(geometrics.length / 2), (axis + 1) % 3, transformation);
        this.bBox = this.left.computeBoundingBox(geometrics).combine(this.right.computeBoundingBox(geometrics));
    }
    this.bBox.set_transformation(geometrics[0].transformation);
}
BvhNode.prototype.computeBoundingBox = function (geometrics) {
    return this.bBox.computeBoundingBox(geometrics);
};
BvhNode.prototype.hit = function (ray, t0, sr) {
    if (this.bBox.hit(ray, t0, sr).hit) {
        var lRec = null,
            rRec = null;
        if (this.left != null)
            lRec = this.left.hit(ray, t0, sr);
        if (this.right != null)
            rRec = this.right.hit(ray, t0, sr);
        if (lRec != null && lRec.hit && rRec != null && rRec.hit) {
            var rec;
            if (lRec.t < rRec.t)
                rec = lRec;
            else
                rec = rRec;
            return rec;
        } else if (lRec != null && lRec.hit)
            return lRec;
        else if (rRec != null && rRec.hit)
            return rRec;
        else {
            return new HitRecord(false, sr, t0);
        }
    }
    return new HitRecord(false, sr, t0);
};
BvhNode.prototype.axisComparator = function (o1, o2) {
    var a, b;
    if (this.axis == 0) {
        a = o1.getCenter().x;
        b = o2.getCenter().x;
    } else if (this.axis == 1) {
        a = o1.getCenter().y;
        b = o2.getCenter().y;
    } else {
        a = o1.getCenter().z;
        b = o2.getCenter().z;
    }
    if (a > b)
        return 1;
    else if (a < b)
        return -1;
    else
        return 0;
};
BvhNode.prototype.getCenter = function () {
    return this.bBox.min.add(this.bBox.max).scale(0.5);
};