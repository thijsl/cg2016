function Tracer(world_ptr) {
    this.world = world_ptr;
}
Tracer.prototype = {
    trace_ray : function(ray, tminOrDepth, depth) {
        var color;
        if (arguments.length == 1) {
            color = new Color(0,0,0);
        } else if (arguments.length == 2) {
            var sr = this.world.hit_bare_bones_objects(ray);
            if (sr.hit_an_object) {
                sr.ray = ray;
                color = sr.material.shade(sr);
            } else {
                color = this.world.background_color;
            }
        } else if (arguments.length == 3) {
            color = this.world.background_color;
        }
        assert(color instanceof Color, "Color in trace_ray isn't a color.");
        return color;
    }
};

function SingleSphere(world_ptr) {
    this.world = world_ptr;
}
SingleSphere.prototype = new Tracer();
SingleSphere.prototype.trace_ray = function(ray) {
    var sr = new ShadeRec();
    var t = 0;
    if (this.world.sphere.hit(ray,t,sr)) {
        return new Color(0,1,1);
    } else {
        return new Color(0,0,0);
    }
};

function MultipleObjects(world_ptr) {
    this.world = world_ptr;
};
MultipleObjects.prototype = new Tracer();
MultipleObjects.prototype.trace_ray = function(ray, depth) {
    var color, t;
    if (arguments.length == 1) {
        color = this.world.background_color;
    } else if (arguments.length == 2) {
        var sr = (this.world.acc === "bvh") ? this.world.hit_bare_bones_objects(ray) : this.world.hit_bare_bones_objects2(ray);
        if (sr.hit_an_object === true) {
            sr.ray = ray;
            color = sr.material.shade(sr);
            t = sr.t;
        } else {
            color = this.world.background_color;
        }
    } else if (arguments.length == 3) {
        color = this.world.background_color;
    }
    assert(color instanceof Color, "Color in trace_ray isn't a color.");
    return {color: color, t: t};
};

function Whitted(world_ptr) {
    this.world = world_ptr;
};
Whitted.prototype = new Tracer();
Whitted.prototype.trace_ray = function(ray, t, depth) {
    if (depth && depth > this.world.vp.max_depth) {
        return {color: new Color(0,0,0), t: t};
    } else {
        var color;
        if (arguments.length == 1) {
            return this.trace_ray(ray, 0);
        } else if (arguments.length == 3) {
            // console.log(ray, t, depth);
            var sr = (this.world.acc === "bvh") ? this.world.hit_bare_bones_objects(ray) : this.world.hit_bare_bones_objects2(ray);
            if (sr.hit_an_object === true) {
                sr.ray = ray;
                sr.depth = depth;
                t = sr.t;
                color = sr.material.shade(sr);
            } else {
                t = Number.MAX_VALUE;
                color = this.world.background_color;
            }
        } else if (arguments.length == 2) {
            return this.trace_ray(ray, 0, arguments[1]);
        }
        assert(color instanceof Color, "Color in trace_ray isn't a color.");
        return {color: color, t: t};
    }
};
