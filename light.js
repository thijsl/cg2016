function Light() {
	this.shadows = true;
	this.intensity = 1.0;
};
Light.prototype = {
	direction : function(sr) {
		return new Vector(0,0,0);
	},
	l : function(sr) {
		return new Vector(0,0,0);
	},
	scale_radiance : function(multiplier) {
		this.intensity = this.intensity*multiplier;
	},
	casts_shadows : function() {
		return this.shadows;
	},
	g : function(sr) {
		return 1;
	},
	pdf : function(sr) {
		return 1;
	},
	set_color : function(color) {
		this.color = color;
	},
	set_shadows : function(visibility) {
		this.shadows = visibility;
	}
};

function PointLight(origin, color, intensity) {
	this.origin = origin;
	this.color = color;
	this.intensity = intensity;
};
PointLight.prototype = new Light();
PointLight.prototype.direction = function(sr) {
	var direction = this.origin.subtract(sr.hit_point).hat();
	assert(direction instanceof Vector, "Direction isn't a vector.");
	return direction;
};
PointLight.prototype.l = function(sr) {
	var l = this.color.scale(this.intensity);
	assert(l instanceof Color, "l isn't a color.");
	return l;
};
PointLight.prototype.in_shadow = function(ray, sr) {
	var t = Number.MAX_VALUE,
		objects = sr.world.objects,
		d = this.origin.euclidean_length(ray.origin),
		j = 0,
		hitRecord;
	for (j; j < objects.length; j++) {
		hitRecord = objects[j].shadow_hit(ray, t, sr);
		if (hitRecord.hit && hitRecord.t < d) {
			return true;
		}
	}
	return false;
};

function AmbientLight() {
	this.color = new Color(1,1,1);
	this.intensity = 1.0;
};
AmbientLight.prototype = new Light();
AmbientLight.prototype.direction = function(sr) {
	return new Vector(0,0,0);
};
AmbientLight.prototype.l = function(sr) {
	var l = this.color.scale(this.intensity);
	assert(l instanceof Color, "l isn't a color.");
	return l;
};

function AreaLight(object, material) {
	this.color = new Color(1,1,1);
	this.intensity = 1.0;
	this.object = object;
    this.material = material;
    this.light_normal = null;
    this.sample_point = null;
}
AreaLight.prototype = new Light();
AreaLight.prototype.direction = function(sr) {
	this.sample_point = this.object.sample();
    this.wi = this.sample_point.subtract(sr.hit_point);
    this.light_normal = this.object.get_normal(this.sample_point);
    return this.wi.normalize();
};
AreaLight.prototype.in_shadow = function(ray, sr) {
    var t = 0,
        num_objects = sr.world.objects.length,
        ts = (this.sample_point.subtract(ray.origin)).dot(ray.direction);
    for (var j = 0; j < num_objects; j++) {
        var hitRecord = sr.world.objects[j].shadow_hit(ray, t, sr);
        if (hitRecord.hit && hitRecord.t < ts) {
            return true
        }
    }
    return false;
};
AreaLight.prototype.l = function(sr) {
	var l,
		ndotd = this.light_normal.negate().dot(this.wi);
    if (ndotd > 0) {
        l = this.material.get_Le(sr);
    } else {
        l = new Color(0,0,0);
    }
	assert(l instanceof Color, "l isn't a color.");
	return l;
};
AreaLight.prototype.g = function(sr) {
    var ndotd = this.light_normal.negate().dot(this.wi),
        d2 = this.sample_point.d_squared(sr.hit_point);
    return (ndotd / d2);
};
AreaLight.prototype.pdf = function(sr) {
    return this.object.pdf(sr);
};
AreaLight.prototype.set_object = function(object) {
	this.object = object;
	this.material = object.material;
};

function AmbientOccluder() {
	this.intensity = 1.0;
}
AmbientOccluder.prototype = new Light();
AmbientOccluder.prototype.set_sampler = function(s_ptr) {
	this.sampler_ptr = s_ptr;
	this.sampler_ptr.map_samples_to_hemisphere(1);
};
AmbientOccluder.prototype.set_min_amount = function(min_amount) {
	this.min_amount = min_amount;
};
AmbientOccluder.prototype.direction = function(sr) {
	var sp = this.sampler_ptr.sample_hemisphere();
	var direction = this.u.scale(sp.x).add(this.v.scale(sp.y)).add(this.w.scale(sp.z));
	return direction;
};
AmbientOccluder.prototype.in_shadow = function(ray, sr) {
	var t,
		num_objects = sr.world.objects.length;

	for (var j = 0; j < num_objects; j++) {
		var hitRecord = sr.world.objects[j].shadow_hit(ray, t, sr);
		if (hitRecord.hit) {
			return true
		}
	}
	return false;
};
AmbientOccluder.prototype.l = function(sr) {
	var l;
	this.w = sr.normal;
	this.v = this.w.cross(new Vector(0.0072, 1, 0.0034)).normalize();
	this.u = this.v.cross(this.w);

	var shadowRay = new Ray(sr.hit_point, this.direction(sr));
	if (this.in_shadow(shadowRay, sr)) {
		l = this.color.scale(this.min_amount*this.intensity);
	}  else {
		l = this.color.scale(this.intensity);
	}
	assert(l instanceof Color, "l isn't a color.");
	return l;
};