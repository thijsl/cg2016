# README #

### What? ###
This is an ongoing project where a raytracer is implemented in Javascript. The Raytracer is based on the instructions of the book "Raytracing from the Ground up".

A class at the KU Leuven, "Computer Graphics: Project", handed out this assignment. The author of this implementation is Thijs Lowette. Contact details: thijs.lowette@student.kuleuven.be

### How to deploy? ###
Pull the whole project and open index.html in your browser. It should be able to run locally without web server. Press the render button to start the raytracing process.

### How to convert <object>.obj to <object>.json? ###
Fire up command prompt in the obj folder, add a <object>.obj file and do the following command:
"javac ObjToJson.java & java ObjToJson <object>" (Windows)
