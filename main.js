function Main(dim, objects, camera, lights, acc, sampler, num_samples, num_sets, scene) {
	var width = dim.width, //parseInt(document.getElementById("width").value),
		height = dim.height, //parseInt(document.getElementById("height").value),
		camera = new PinholeCamera(this, new Point(camera.eye.x,camera.eye.y,camera.eye.z),
			new Point(camera.lookat.x,camera.lookat.y,camera.lookat.z), new Vector(camera.up.x, camera.up.y, camera.up.z), 1, 90, 1),
		lights = lights.map(function(light){return new PointLight(new Point(light.x, light.y, light.z), new Color(1,1,1), light.intensity)})
			.filter(function(light){return light.intensity > 0}),
		vp = new ViewPlane(),
		background_color = BACKGROUND_COLOR,
		world = new World(vp, objects, background_color, camera, lights, acc),
		sampler = {method: sampler, num_samples: num_samples, num_sets: num_sets},
		dimension = (!scene || scene === 'default') ? world['build'](width,height, dim, sampler) : world['build_'+scene](width,height, dim, sampler);

	if (dimension) {
		world.render_scene();
		postMessage({timeUpdate: new Date()});
	}
};

function assert(condition, message, value) {
    if (!condition) {
		console.log("Value:", value);
        throw message || "Assertion failed";
    }
}
