function Material() {

};
Material.prototype = {
    shade: function (sr) {
        return BACKGROUND_COLOR;
    },
    path_shade: function (sr) {
        return BACKGROUND_COLOR;
    },
    shadow: true
};

function Matte() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
}
Matte.prototype = new Material();
Matte.prototype.shade = function (sr) {
    var wo = sr.ray.direction.negate(),
        L = sr.world.ambient_light.l(sr).multiply(this.ambient_brdf.rho(sr, wo)),
        numLights = sr.world.lights.length;
    for (var j = 0; j < numLights; j++) {
        var light = sr.world.lights[j],
            wi = light.direction(sr),
            ndotwi = sr.normal.dot(wi);
        if (ndotwi > 0.0) {
            if (light.casts_shadows()) {
                var shadowRay = new Ray(sr.hit_point, wi),
                    in_shadow = light.in_shadow(shadowRay, sr);
            }
            if (!in_shadow) {
                var diffuseC = this.diffuse_brdf.f(sr, wo, wi),
                    illum = light.l(sr),
                    g = light.g(sr),
                    pdf = light.pdf(sr);
                L = L.add(diffuseC.multiply(illum).scale(g * ndotwi / pdf));
            }
        }
    }
    return L;
};
// ambient reflection coefficient
Matte.prototype.set_ka = function (ka) {
    this.ambient_brdf.kd = ka;
};
// diffuse reflection coefficient
Matte.prototype.set_kd = function (kd) {
    this.diffuse_brdf.kd = kd;
};
// diffuse color
Matte.prototype.set_cd = function (cd) {
    this.ambient_brdf.cd = cd;
    this.diffuse_brdf.cd = cd;
};

function SV_Matte() {
    this.ambient_brdf = new SV_Lambertian();
    this.diffuse_brdf = new SV_Lambertian();
}
SV_Matte.prototype = new Material();
SV_Matte.prototype.shade = function (sr) {
    var wo = sr.ray.direction.scale(-1),
        L = sr.world.ambient_light.l(sr).multiply(this.ambient_brdf.rho(sr, wo)),
        numLights = sr.world.lights.length;
    for (var j = 0; j < numLights; j++) {
        var light = sr.world.lights[j],
            wi = light.direction(sr),
            ndotwi = sr.normal.dot(wi),
            ndotwo = sr.normal.dot(wo);
        wi = wi.normalize();
        if (ndotwi > 0.0 && ndotwo > 0.0) {
            var inShadow = false;
            if (light.casts_shadows()) {
                var sRay = new Ray(sr.hit_point, wi);
                inShadow = light.in_shadow(sRay, sr);
            }
            if (!inShadow || !this.shadow) {
                L = L.add(this.diffuse_brdf.f(sr, wo, wi).multiply(light.l(sr))
                    .scale(light.g(sr) * ndotwi / light.pdf(sr)));
            }
        }
    }
    return L;
};
// ambient reflection coefficient
SV_Matte.prototype.set_ka = function (ka) {
    this.ambient_brdf.kd = ka;
};
// diffuse reflection coefficient
SV_Matte.prototype.set_kd = function (kd) {
    this.diffuse_brdf.kd = kd;
};
// diffuse color
SV_Matte.prototype.set_cd = function (texture) {
    this.ambient_brdf.cd = texture;
    this.diffuse_brdf.cd = texture;
};

function Phong() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    this.specular_brdf = new GlossySpecular();
}
Phong.prototype = new Material();
Phong.prototype.shade = function (sr) {
    var wo = sr.ray.direction.negate(),
        L = this.ambient_brdf.rho(sr, wo).multiply(sr.world.ambient_light.l(sr)),
        numLights = sr.world.lights.length;
    for (var j = 0; j < numLights; j++) {
        var light = sr.world.lights[j],
            wi = light.direction(sr),
            ndotwi = sr.normal.dot(wi),
            in_shadow = false,
            shadowRay;
        if (ndotwi > 0.0) {
            if (light.casts_shadows()) {
                shadowRay = new Ray(sr.hit_point, wi);
                in_shadow = light.in_shadow(shadowRay, sr);
            }
            if (!in_shadow) {
                var diffuseC = this.diffuse_brdf.f(sr, wo, wi),
                    specC = this.specular_brdf.f(sr, wo, wi),
                    illum = light.l(sr),
                    g = light.g(sr),
                    pdf = light.pdf(sr);
                L = L.add(diffuseC.add(specC).multiply(illum).scale(g * ndotwi / pdf));
            } else {
                // console.log("wel in shadow");
            }

        }
    }
    return L;
};
// ambient reflection coefficient
Phong.prototype.set_ka = function (ka) {
    this.ambient_brdf.kd = ka;
};
// diffuse reflection coefficient
Phong.prototype.set_kd = function (kd) {
    this.diffuse_brdf.kd = kd;
};
// diffuse color
Phong.prototype.set_cd = function (cd) {
    this.ambient_brdf.cd = cd;
    this.diffuse_brdf.cd = cd;
};
Phong.prototype.set_ks = function (ks) {
    this.specular_brdf.ks = ks;
};
Phong.prototype.set_cs = function (cs) {
    this.specular_brdf.cs = cs;
};
Phong.prototype.set_exp = function (exp) {
    this.specular_brdf.exp = exp;
};

function CookTorranceM() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    this.specular_brdf = new CookTorrance(1.5, 1, 0.117, 0.0137);
    var diffuseColor = new Color(0.0143,0.033,0.0639),
        specularColor = new Color(0.0291,0.0193,0.0118);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
CookTorranceM.prototype = new Phong();
CookTorranceM.prototype.initialise = function () {
    this.set_ka(1);
    this.set_ks(0.8);
    this.set_kd(0.8);
};
CookTorranceM.prototype.shade = function (sr) {
    var wo = sr.ray.direction.negate(),
        L = this.ambient_brdf.rho(sr, wo).multiply(sr.world.ambient_light.l(sr)),
        numLights = sr.world.lights.length;
    for (var j = 0; j < numLights; j++) {
        var light = sr.world.lights[j],
            wi = light.direction(sr),
            ndotwi = sr.normal.dot(wi),
            in_shadow = false,
            shadowRay;
        if (ndotwi > 0.0) {
            if (light.casts_shadows()) {
                shadowRay = new Ray(sr.hit_point, wi);
                in_shadow = light.in_shadow(shadowRay, sr);
            }
            if (!in_shadow) {
                var diffuseC = this.diffuse_brdf.f(sr, wo, wi),
                    specC = this.specular_brdf.f(sr, wo, wi),
                    illum = light.l(sr),
                    g = light.g(sr),
                    pdf = light.pdf(sr);
                L = L.add(diffuseC.add(specC).multiply(illum).scale(g * ndotwi / pdf));
            } else {
                // console.log("wel in shadow");
            }

        }
    }
    return L;
};

function AcrylicBlue() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    var diffuseColor = new Color(0.0143,0.033,0.0639),
        specularColor = new Color(0.0291,0.0193,0.0118);
    this.specular_brdf = new CookTorrance(1.5, 1, 0.117, 0.0137);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
AcrylicBlue.prototype = new CookTorranceM();

function BlueRubber() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    var diffuseColor = new Color(0.0382, 0.0669, 0.094),
        specularColor = new Color(0.247, 0.204, 0.136);
    this.specular_brdf = new CookTorrance(1.5, 1, 0.0366, 0.276);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
BlueRubber.prototype = new CookTorranceM();

function SiliconNitrade() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    var diffuseColor = new Color(0.0119, 0.00894, 0.00572),
        specularColor = new Color(0.0234, 0.0175, 0.0145);
    this.specular_brdf = new CookTorrance(1.5, 1, 0.526, 0.00712);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
SiliconNitrade.prototype = new CookTorranceM();

function PaintLightRed() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    var diffuseColor = new Color(0.414, 0.037, 0.00602),
        specularColor = new Color(0.192, 0.147, 0.103);
    this.specular_brdf = new CookTorrance(1.5, 1, 0.0689, 0.316);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
PaintLightRed.prototype = new CookTorranceM();

function WhiteBbal() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    var diffuseColor = new Color(0.282, 0.194, 0.0733),
        specularColor = new Color(0.0321, 0.0243, 0.0185);
    this.specular_brdf = new CookTorrance(1.5, 1, 0.184, 0.0164);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
WhiteBbal.prototype = new CookTorranceM();

function WhitePaint() {
    this.ambient_brdf = new Lambertian();
    this.diffuse_brdf = new Lambertian();
    var diffuseColor = new Color(0.317, 0.269, 0.181),
        specularColor = new Color(0.183, 0.137, 0.101);
    this.specular_brdf = new CookTorrance(1.5, 1, 0.0145, 0.0386);
    this.set_cd(diffuseColor);
    this.set_cs(specularColor);
    this.initialise();
}
WhitePaint.prototype = new CookTorranceM();

function Dielectric(cf_in, cf_out) {
    this.fresnel_BRDF = new FresnelReflector();
    this.fresnel_BTDF = new FresnelTransmitter();
    this.cf_in = cf_in;
    this.cf_out = cf_out;
}
Dielectric.prototype = new Phong();
Dielectric.prototype.set_cf_in = function (c) {
    assert(c instanceof Color, "set_cf_in isn't a color.");
    this.cf_in = c;
};
Dielectric.prototype.set_cf_out = function (c) {
    assert(c instanceof Color, "set_cf_out isn't a color.");
    this.cf_out = c;
};
Dielectric.prototype.set_eta_in = function (eta) {
    this.fresnel_BRDF.set_eta_in(eta);
    this.fresnel_BTDF.set_eta_in(eta);
};
Dielectric.prototype.set_eta_out = function (eta) {
    this.fresnel_BRDF.set_eta_out(eta);
    this.fresnel_BTDF.set_eta_out(eta);
};
Dielectric.prototype.set_fresnel_BRDF = function (fresnel_BRDF) {
    this.fresnel_BRDF = fresnel_BRDF;
};
Dielectric.prototype.set_fresnel_BTDF = function (fresnel_BTDF) {
    this.fresnel_BTDF = fresnel_BTDF;
};
Dielectric.prototype.shade = function (sr) {
    var l = Phong.prototype.shade.call(this, sr),
        wi = new Vector(),
        wo = sr.ray.direction.negate(),
        fr_sample = this.fresnel_BRDF.sample_f(sr, wo, wi),
        fr = fr_sample.l;
    wi = fr_sample.wi;
    var reflectedRay = new Ray(sr.hit_point, wi),
        Lr = new Color(0,0,0),
        Lt = new Color(0,0,0),
        ndotwi = sr.normal.dot(wi),
        t = Number.MAX_VALUE;
    if (this.fresnel_BTDF.tir(sr)) {
        var traced_ray = sr.world.tracer_ptr.trace_ray(reflectedRay, t, sr.depth+1);
        t = traced_ray.t;
        Lr = traced_ray.color;
        if (ndotwi < 0) {
            l = l.add(this.cf_in.pow(t).multiply(Lr));
        } else {
            l = l.add(this.cf_out.pow(t).multiply(Lr));
        }
    } else {
        var sampled_f = this.fresnel_BTDF.sample_f(sr, wo, new Vector(0,0,0)),
            wt = sampled_f.wi,
            ft = sampled_f.l,
            transmittedRay = new Ray(sr.hit_point, wt),
            ndotwt = sr.normal.dot(wt);
        if (ndotwi < 0) {
            var traced_ray = sr.world.tracer_ptr.trace_ray(reflectedRay, t, sr.depth+1);
            t = traced_ray.t;
            Lr = traced_ray.color.scale(Math.abs(ndotwi)).multiply(fr);
            l = l.add(this.cf_in.pow(t).multiply(Lr));
            var traced_ray2 = sr.world.tracer_ptr.trace_ray(transmittedRay, t, sr.depth+1);
            t = traced_ray2.t;
            Lt = traced_ray2.color.scale(Math.abs(ndotwt)).multiply(ft);
            l = l.add(this.cf_out.pow(t).multiply(Lt));
        } else{
            var traced_ray = sr.world.tracer_ptr.trace_ray(reflectedRay, t, sr.depth+1);
            t = traced_ray.t;
            Lr = traced_ray.color.scale(Math.abs(ndotwi)).multiply(fr);
            l = l.add(this.cf_out.pow(t).multiply(Lr));
            var traced_ray2 = sr.world.tracer_ptr.trace_ray(transmittedRay, t, sr.depth+1);
            t = traced_ray2.t;
            Lt = traced_ray2.color.scale(Math.abs(ndotwt)).multiply(ft);
            l = l.add(this.cf_in.pow(t).multiply(Lt));
        }
    }
    assert(l instanceof Color, "Dielectric shade does not produce a color.");
    return l;
};

function Emissive(ls, ce) {
    this.ls = ls; // float
    this.ce = ce; // color
}
Emissive.prototype = new Material();
Emissive.prototype.shade = function (sr) {
    if (sr.normal.dot(sr.ray.direction) > 0) { // TODO: altijd negatief, bij *-1 heel veel ruis
        return this.ce.scale(this.ls);
    } else {
        return new Color(0, 0, 0); // black
    }
};
Emissive.prototype.get_Le = function (sr) {
    return this.shade(sr);
};
Emissive.prototype.scale_radiance = function (ls) {
    return this.ls = ls;
};
Emissive.prototype.set_ce = function (ce) {
    return this.ce = ce;
};