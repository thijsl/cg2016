function Camera(world, eye, lookat, up, exposure_time) {
    this.world = world;
    this.eye = eye; // Point3D
    this.lookat = lookat; // Point3D
    this.up = up; // Vector3D
    this.exposure_time = exposure_time; // float
};
Camera.prototype.computeUvw = function () {
    if (this.eye.x == this.lookat.x && this.eye.z == this.lookat.z && this.eye.y < this.lookat.y) {
        //when looking straight up
        this.w = new Vector(0, -1, 0);
        this.v = this.up.normalize();
        this.u = this.v.cross(this.w).normalize();

    } else if (this.eye.x == this.lookat.x && this.eye.z == this.lookat.z && this.eye.y > this.lookat.y) {
        //when looking straight down
        this.w = new Vector(0, 1, 0);
        this.v = this.up.normalize();
        this.u = this.v.cross(this.w).normalize();
    } else {
        this.w = this.eye.subtract(this.lookat).normalize();
        this.u = this.up.cross(this.w).normalize();
        this.v = this.w.cross(this.u).normalize();
    }
};

function PinholeCamera(world, eye, lookat, up, exposure_time, d, zoom) {
    this.world = world;
    this.eye = eye;
    this.lookat = lookat;
    this.up = up;
    this.exposure_time = exposure_time;
    this.d = d;
    this.zoom = zoom;
};
PinholeCamera.prototype = new Camera();
PinholeCamera.prototype.render_scene = function (world) {
    var L = new Color(0.8, 0.8, 0.8),
        vp = world.vp,
        ray = new Ray(),
        depth = 0,
        sp, r, c, x, y, j;
    vp.s = vp.s / this.zoom;
    ray.origin = this.eye;
    for (r = world.dim.rStart; r < world.dim.rEnd; r++) {
        for (c = world.dim.cStart; c < world.dim.cEnd; c++) {
            L = BACKGROUND_COLOR;
            for (j = 0; j < vp.num_samples; j++) {
                sp = vp.sampler.sample_unit_square();
                x = vp.s * (c - 0.5 * vp.hres + sp.x);
                y = vp.s * (r - 0.5 * vp.vres + sp.y);
                ray.direction = this.ray_direction(x, y);
                var tracedRay = world.tracer_ptr.trace_ray(ray, depth),
                    pixelColor = tracedRay.color;
                L = L.add(pixelColor);
            }
            L = L.scale(1 / vp.num_samples).scale(this.exposure_time);
            world.display_pixel(r, c, L);
        }
        postMessage({timeUpdate: new Date()});
    }
};
PinholeCamera.prototype.ray_direction = function (x, y) {
    var dir = this.u.scale(x).add(this.v.scale(y)).subtract(this.w.scale(this.d));
    return dir.normalize();
};
