function Ray(origin, direction) {
	this.origin = origin;
	this.direction = direction;
}

function ViewPlane(hres, vres, s, gamma, inv_gamma) {
	this.hres = hres;
	this.vres = vres;
	this.s = s;
	this.gamma = gamma;
	this.inv_gama = inv_gamma;
	this.num_samples = 10;
	this.max_depth = 5;
}

ViewPlane.prototype = {
	set_sampler : function(sampler) {
		this.num_samples = sampler.num_samples;
		this.sampler = sampler;
	},
	set_max_depth : function(depth) {
		this.max_depth = depth;
	}
};

function World(vp, objects, background_color, camera, lights, acc) {
	this.vp = vp;
	this.objects = objects;
	this.background_color = background_color;
	if (camera) {
		camera.computeUvw();
	}
	this.camera = camera;
	this.ambient_light = new AmbientLight();
	this.lights = lights;
	this.acc = acc;
}
World.prototype = {
	display_pixel : function(row, column, color) {
		color = this.maxToOne(color);
		postMessage({drawPixel: {row: row, column: column, color: color}});
	},
	maxToOne : function(color) {
		if (this.vp.show_out_of_gamut) {
			if (color.r > 1 || color.g > 1 || color.b > 1) {
				color = new Color(1,0,0);
			}
		} else {
			var max = Math.max(color.r, color.g, color.b);
			color = (max > 1) ?  color.scale(1/max) : color;
		}
		if (this.vp.gamma !== 1.0) {
			color = color.pow(this.vp.inv_gama);
		}
		return color;

	},
	set_ambient_light : function(light) {
		this.ambient_light = light;
	},
	build : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);
		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte(),
			matte3 = new Matte(),
			sphereColor = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));
		matte3.set_ka(0.25);
		matte3.set_kd(0.65);
		matte3.set_cd(new Color(1,1,0.75));
		sphereColor.set_ka(0.25);
		sphereColor.set_kd(5);
		sphereColor.set_cd(new Color(0,0,1));
		// PHONG
		var phong = new Phong(),
			phong2 = new Phong(),
			sphereColorPhong = new Phong();
		phong.set_ka(0.25);
		phong.set_kd(0.65);
		phong.set_cd(new Color(1,1,0));
		phong.set_ks(0.05);
		phong.set_cs(new Color(0.2,0.2,0.2));
		phong.set_exp(2);
		phong2.set_ka(0.25);
		phong2.set_kd(0.65);
		phong2.set_cd(new Color(1,1,1));
		phong2.set_ks(0.05);
		phong2.set_cs(new Color(1,1,1));
		phong2.set_exp(2);
		sphereColorPhong.set_ka(0.25);
		sphereColorPhong.set_kd(0.65);
		sphereColorPhong.set_cd(new Color(0,0,1));
		sphereColorPhong.set_ks(0.9);
		sphereColorPhong.set_cs(new Color(1,1,1));
		sphereColorPhong.set_exp(20);

		// var occluder_ptr = new AmbientOccluder();
		// occluder_ptr.scale_radiance(1.0);
		// occluder_ptr.set_color(new Color(1,1,1));
		// occluder_ptr.set_min_amount(0.0);
		// occluder_ptr.set_sampler(sampler_ptr);
		// this.set_ambient_light(occluder_ptr);

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(matte3);
			this.objects[i].set_transformation(t4);
		}


		var rectangle = new Rectangle(new Point(0, 10, 0), new Vector(1, 0, 0), new Vector(0, 0, 1));
		var emissive = new Emissive();
		emissive.scale_radiance(40.0);
		emissive.set_ce(new Color(1,1,1));
		rectangle.set_material(emissive);
		rectangle.set_shadows(false);
		rectangle.set_sampler(sampler_ptr);
		rectangle.set_transformation(t4);
		this.add_object(rectangle);
		var area_light = new AreaLight();
		area_light.set_object(rectangle);
		area_light.set_shadows(true);
		this.add_light(area_light);

		var sphere = new Sphere(new Point(0,0,0), 1);
		sphere.set_material(sphereColorPhong);
		sphere.set_transformation(t4);
		this.add_object(sphere);

		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
	render_scene : function() {
		this.camera.render_scene(this);
	},
	add_object : function(object) {
		this.objects.push(object);
	},
	add_light : function(light) {
		this.lights.push(light);
	},
	hit_bare_bones_objects2 : function(ray) {
		var sr = new ShadeRec(this),
			t = 0,
			normal = new Vector(0,0,0),
			local_hit_point = new Point(0,0,0),
			tmin = Number.MAX_VALUE, // kHugeValue
			num_objects = this.objects.length,
			j,
			hitRecord;
		for (j = 0; j < num_objects; j++) {
			hitRecord = this.objects[j].hit(ray, t, sr);
			t = hitRecord.t;
			if (hitRecord.hit && (t < tmin)) {
				sr.hit_an_object = true;
				tmin = t;
				sr.material = this.objects[j].material;
				sr.hit_point = ray.direction.scale(t).add(ray.origin);
				normal = hitRecord.sr.normal;
				local_hit_point = hitRecord.sr.local_hit_point;
			}
		}
		if (sr.hit_an_object) {
			sr.t = tmin;
			sr.normal = normal;
			sr.local_hit_point = local_hit_point;
		}
		return sr;

	},
	hit_bare_bones_objects : function(ray) {
		var sr = new ShadeRec(this);
		var t = 0,
			hitRecord = this.bvhNode.hit(ray, t, sr);
		if (hitRecord.hit && (hitRecord.t < Number.MAX_VALUE)) {
			hitRecord.sr.hit_point = ray.direction.scale(hitRecord.t).add(ray.origin);
		}
		return hitRecord.sr;
	},
	create_sampler_by_name : function(name, num_samples, num_sets) {
		var sampler_ptr;
		switch (name) {
			case "jittered" :
				sampler_ptr = new Jittered(num_samples, num_sets);
				break;
			case "regular" :
				sampler_ptr = new Regular(num_samples, num_sets);
				break;
			case "pureRandom" :
				sampler_ptr = new PureRandom(num_samples, num_sets);
				break;
			case "multiJittered" :
				sampler_ptr = new MultiJittered(num_samples, num_sets);
				break;
			case "nRooks" :
				sampler_ptr = new NRooks(num_samples, num_sets);
				break;
			case "hammersley" :
				sampler_ptr = new Hammersley(num_samples, num_sets);
				break;
			default :
				sampler_ptr = new Jittered(num_samples, num_sets);
				break;
		}
		return sampler_ptr
	},
	build_matte_basic : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);
		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte(),
			matte = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));
		matte.set_ka(0.25);
		matte.set_kd(0.65);
		matte.set_cd(new Color(1,1,0.75));

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(matte);
			this.objects[i].set_transformation(t4);
		}

		var sphere = new Sphere(new Point(0,0,0), 1);
		sphere.set_material(matte);
		sphere.set_transformation(t4);
		this.add_object(sphere);

		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
	build_phong_basic : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);

		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));

		// PHONG
		var phong = new Phong();
		phong.set_ka(0.25);
		phong.set_kd(0.65);
		phong.set_cd(new Color(0,0,1));
		phong.set_ks(0.9);
		phong.set_cs(new Color(1,1,1));
		phong.set_exp(15);

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(phong);
			this.objects[i].set_transformation(t4);
		}

		var sphere = new Sphere(new Point(0,0,0), 1);
		sphere.set_material(phong);
		sphere.set_transformation(t4);
		this.add_object(sphere);

		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
	build_geometries : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);

		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));

		// PHONG
		var phong = new Phong();
		phong.set_ka(0.25);
		phong.set_kd(0.65);
		phong.set_cd(new Color(0,0,1));
		phong.set_ks(0.9);
		phong.set_cs(new Color(1,1,1));
		phong.set_exp(15);

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(phong);
			this.objects[i].set_transformation(t4);
		}

		// var sphere = new Sphere(new Point(0,0,0), 1);
		// sphere.set_material(phong);
		// sphere.set_transformation(t4);
		// this.add_object(sphere);

		if (this.objects.length < 20) {
			var cylinder = new OpenCylinder(-0.25,0.25,1);
			cylinder.set_material(phong);
			cylinder.set_transformation(t4);
			this.add_object(cylinder);
		}

		// var beam = new Beam(new Point(1,1,1),new Point(2,2,2));
		// beam.set_material(phong);
		// beam.set_transformation(t4);
		// this.add_object(beam);

		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
	build_textures : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);

		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));

		var image = earth,
			sphericalMap = new SphericalMap(),
			texture = new ImageTexture(image.hres, image.vres, image, sphericalMap),
			svEmissive = new SV_Matte();// textured material:
		svEmissive.set_ka(1.0);
		svEmissive.set_cd(texture);

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(svEmissive);
			this.objects[i].set_transformation(t4);
		}

		var sphere = new Sphere(new Point(0,0,0), 1);
		sphere.set_material(svEmissive);
		sphere.set_transformation(t4);
		this.add_object(sphere);

		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		var rectangle = new Rectangle(new Point(0, 10, 0), new Vector(1, 0, 0), new Vector(0, 0, 1));
		var emissive = new Emissive();
		emissive.scale_radiance(40.0);
		emissive.set_ce(new Color(1,1,1));
		rectangle.set_material(emissive);
		rectangle.set_shadows(false);
		rectangle.set_sampler(sampler_ptr);
		rectangle.set_transformation(t4);
		this.add_object(rectangle);
		var area_light = new AreaLight();
		area_light.set_object(rectangle);
		area_light.set_shadows(true);
		this.add_light(area_light);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
	build_cook_torrance : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);

		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));

		var siliconNitrade = new SiliconNitrade();

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(siliconNitrade);
			this.objects[i].set_transformation(t4);
		}

		var sphere = new Sphere(new Point(0,0,0), 1);
		sphere.set_material(siliconNitrade);
		sphere.set_transformation(t4);
		this.add_object(sphere);


		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		var rectangle = new Rectangle(new Point(0, 10, 0), new Vector(1, 0, 0), new Vector(0, 0, 1));
		var emissive = new Emissive();
		emissive.scale_radiance(40.0);
		emissive.set_ce(new Color(1,1,1));
		rectangle.set_material(emissive);
		rectangle.set_shadows(false);
		rectangle.set_sampler(sampler_ptr);
		rectangle.set_transformation(t4);
		this.add_object(rectangle);
		var area_light = new AreaLight();
		area_light.set_object(rectangle);
		area_light.set_shadows(true);
		this.add_light(area_light);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		this.lights[0].intensity = 60;
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
	build_transparant : function(hres, vres, dim, sampler) {
		this.vp.hres = hres;
		this.vp.vres = vres;
		var sampler_ptr = this.create_sampler_by_name(sampler.method, sampler.num_samples, sampler.num_sets);
		this.vp.set_sampler(sampler_ptr);
		this.dim = dim;
		this.vp.s = 1.0;
		this.vp.gamma = 1.0;
		this.vp.inv_gama  = 1/this.vp.gamma;
		this.background_color = BACKGROUND_COLOR;
		this.tracer_ptr = new Whitted(this);

		// MATTE
		var groundColor = new Matte(),
			wallColor = new Matte();
		groundColor.set_ka(0.25);
		groundColor.set_kd(0.65);
		groundColor.set_cd(new Color(1,0,0));
		wallColor.set_ka(0.25);
		wallColor.set_kd(5);
		wallColor.set_cd(new Color(0,1,0));

		var glassColor = new Color(0.64, 0.98, 0.88),
			white = new Color(1,1,1),
			dielectric = new Dielectric();
		dielectric.set_ks(0.2);
		dielectric.set_exp(2000);
		dielectric.set_eta_in(1.5);
		dielectric.set_eta_out(1.0);
		dielectric.set_cf_in(glassColor);
		dielectric.set_cf_out(white);

		// OBJECTS
		var t0 = new Transformation(),
			t4 = t0.translate(2.5, 2.5, 0.0),
			noTranslation = t0.translate(0, 0, 0);

		for (var i = 0; i < this.objects.length; i++) {
			this.objects[i].set_material(dielectric);
			this.objects[i].set_transformation(t4);
		}

		var sphere = new Sphere(new Point(0,0,0), 1);
		sphere.set_material(dielectric);
		sphere.set_transformation(t4);
		this.add_object(sphere);

		var ground = new Plane(new Point(0,0,0), new Vector(0,1,0)), //xz-plane ('ground')
			wall = new Plane(new Point(0,0,0), new Vector(1,0,0)); //yz-plane
		ground.set_material(groundColor);
		wall.set_material(wallColor);
		ground.set_transformation(noTranslation);
		wall.set_transformation(noTranslation);
		this.add_object(ground);
		this.add_object(wall);

		var rectangle = new Rectangle(new Point(0, 10, 0), new Vector(1, 0, 0), new Vector(0, 0, 1));
		var emissive = new Emissive();
		emissive.scale_radiance(40.0);
		emissive.set_ce(new Color(1,1,1));
		rectangle.set_material(emissive);
		rectangle.set_shadows(false);
		rectangle.set_sampler(sampler_ptr);
		rectangle.set_transformation(t4);
		this.add_object(rectangle);
		var area_light = new AreaLight();
		area_light.set_object(rectangle);
		area_light.set_shadows(true);
		this.add_light(area_light);

		// LIGHTS
		this.ambient_light.scale_radiance(1.0);
		if (this.objects.length === 0) {
			postMessage({error: "Geen objecten om te renderen."});
		} else {
			if (this.acc === "bvh") {
				this.bvhNode = new BvhNode(this.objects, 0, noTranslation);
			}
			return [this.vp.hres, this.vp.vres];
		}
	},
};

