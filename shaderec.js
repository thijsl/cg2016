function ShadeRec(param) {
	if (param instanceof World) {
		this.hit_an_object = false;
		this.material = null;
		this.hit_point = new Point(0,0,0);
		this.local_hit_point = new Point(0,0,0);
		this.normal = new Vector(0,0,0);
	  	this.ray = new Ray(new Point(0,0,0), new Vector(0,0,0));
	  	this.depth = 0;
	  	this.dir = new Vector(0,0,0);
		this.color = new Color(0,0,0);
		this.world = param;
	} else if (param instanceof ShadeRec) {
		this.hit_an_object = param.hit_an_object;
		this.material = param.material;
		this.hit_point = param.hit_point;
		this.local_hit_point = param.local_hit_point;
		this.normal = param.normal;
		this.ray = param.ray;
		this.depth = param.depth;
		this.dir = param.dir;
		this.world = param.world;
	}
};

function HitRecord(hit, sr, t) {
	this.hit = hit;
	this.sr = sr;
	this.t = t;
}
