import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ObjToJson {
	
	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++) {
			String triangles = readFile(args[i]);
			writeFile(args[i], triangles);
		}
	}
	
	public static String readFile(String fileName) {
		ArrayList<String> lines = new ArrayList<String>();
		ArrayList<String[]> vertices = new ArrayList<String[]>();
		ArrayList<String[]> normals = new ArrayList<String[]>();
		ArrayList<String[]> textures = new ArrayList<String[]>();
		String triangles = fileName + " = '[";
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(fileName+".obj"));
			while ((line = br.readLine()) != null) {
				String[] els = line.split(" ");
				if (els[0].equals("v")) {
					String[] value = {els[1],els[2],els[3]};
					vertices.add(value);
				} else if (els[0].equals("n")) {
					String[] value = {els[1],els[2],els[3]};
					normals.add(value);
				} else if (els[0].equals("vt")) {
					String[] value = {els[1],els[2]};
					textures.add(value);
				} else if (els[0].equals("f")) {
					String[] one = els[1].split("/"),
							two = els[2].split("/"),
							three = els[3].split("/");
					String[] value = {els[1],els[2],els[2]};
					String[] va = vertices.get(Integer.parseInt(one[0])-1),
							 vb = vertices.get(Integer.parseInt(two[0])-1),
							 vc = vertices.get(Integer.parseInt(three[0])-1),
							 na = vertices.get(Integer.parseInt(one[2])-1),
							 nb = vertices.get(Integer.parseInt(two[2])-1),
							 nc = vertices.get(Integer.parseInt(three[2])-1),
							 ta = vertices.get(Integer.parseInt(one[2])-1),
							 tb = vertices.get(Integer.parseInt(two[2])-1),
							 tc = vertices.get(Integer.parseInt(one[2])-1);
					String triangle = "{\"vertices\": {\"a\": {\"x\": "+va[0]+", \"y\": "+va[1]+", \"z\": "+va[2]+"}, \"b\": {\"x\": "+vb[0]+", \"y\": "+vb[1]+", \"z\": "+vb[2]+"}, \"c\": {\"x\": "+vc[0]+", \"y\": "+vc[1]+", \"z\": "+vc[2]+"}}" +
									 ", \"normals\": {\"a\": {\"x\": "+na[0]+", \"y\": "+na[1]+", \"z\": "+na[2]+"}, \"b\": {\"x\": "+nb[0]+", \"y\": "+nb[1]+", \"z\": "+nb[2]+"}, \"c\": {\"x\": "+nc[0]+", \"y\": "+nc[1]+", \"z\": "+nc[2]+"}}" +
									 ", \"textures\": {\"a\": {\"x\": "+ta[0]+", \"y\": "+ta[1]+"}, \"b\": {\"x\": "+tb[0]+", \"y\": "+tb[1]+"}, \"c\": {\"x\": "+tc[0]+", \"y\": "+tc[1]+"}}},";
					triangles += triangle;
				} 
				lines.add(line);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		triangles = triangles.substring(0, triangles.length()-1) + "]';";
		return triangles;
	}
	
	public static void writeFile(String name, String triangles) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(name+".json", "UTF-8");
			writer.println(triangles);	
			writer.close();
			System.out.println("Succesfully converted " + name + ".");
		} catch (FileNotFoundException | UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();		
		}
	}

}
