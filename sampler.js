function Sampler(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.samples = [];
    this.shuffled_indices = [];
    this.count = 0;
    this.jump = 0;
    this.hemisphere_samples = [];
};
Sampler.prototype = {
    generate_samples: function () {
        return 0;
    },
    setup_shuffled_indices: function () {
        return;
    },
    shuffle_samples: function () {
        return;
    },
    sample_unit_square: function () {
        var index = Math.min(this.count++ % (this.num_samples * this.num_sets), this.samples.length - 1);
        return (this.samples[index]);
    },
    sample_hemisphere: function() {
      if ((this.count % this.num_samples) === 0) {
          this.jump = ((Math.floor(Math.random() * Number.MAX_VALUE) + 1) % this.num_sets) * this.num_samples;
      }
      if (this.count < 0) {
          this.count = 0;
      }
      var sampled_hemisphere = (this.hemisphere_samples[(this.jump)]);
      return sampled_hemisphere;
    },
    // chapter 7
    map_samples_to_hemisphere: function (e) {
        var size = this.samples.length,
            hemisphere_samples = [];
        for (var j = 0; j < size; j++) {
            var cos_phi = Math.cos(2.0 * Math.PI * this.samples[j].x),
                sin_phi = Math.sin(2.0 * Math.PI * this.samples[j].x),
                cos_theta = Math.pow((1.0 - this.samples[j].y), 1.0 / (e + 1.0)),
                sin_theta = Math.sqrt(1.0 - cos_theta * cos_theta),
                pu = sin_theta * cos_phi,
                pv = sin_theta * sin_phi,
                pw = cos_theta;
            hemisphere_samples.push(new Point(pu, pv, pw));
        }
        this.hemisphere_samples = hemisphere_samples;
    }
};

function PureRandom(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.generate_samples();
}
PureRandom.prototype = new Sampler();
PureRandom.prototype.generate_samples = function () {
    var n = parseInt(Math.sqrt(this.num_samples)),
        p, j, k,
        sp;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                sp = new Point2D(Math.rand_double2(), Math.rand_double2());
                this.samples.push(sp);
            }
        }
    }
};

function Regular(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.generate_samples();
}
Regular.prototype = new Sampler();
Regular.prototype.generate_samples = function () {
    var n = parseInt(Math.sqrt(this.num_samples)),
        p, j, k,
        sp;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                sp = new Point2D((k + 0.5) / n, (j + 0.5) / n);
                this.samples.push(sp);
            }
        }
    }
};

function Jittered(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.generate_samples();
}
Jittered.prototype = new Sampler();
Jittered.prototype.generate_samples = function () {
    var n = parseInt(Math.sqrt(this.num_samples)),
        p, j, k,
        sp;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                sp = new Point2D((k + Math.rand_double2()) / n, (j + Math.rand_double2()) / n);
                this.samples.push(sp);
            }
        }
    }
};

function Hammersley(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.generate_samples();
}
Hammersley.prototype = new Sampler();
Hammersley.prototype.generate_samples = function () {
    var p, j,
        sp;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j <  this.num_samples; j++) {
            sp = new Point2D(j / this.num_samples, this.phi(j));
            this.samples.push(sp);
        }
    }
};
Hammersley.prototype.phi = function (j) {
    var x = 0.0,
        f = 0.5;

    while (parseInt(j) !== 0) {
        x += f * (j % 2);
        j /= 2;
        f *= 0.5;
    }

    return x;
};

function MultiJittered(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.generate_samples();
}
MultiJittered.prototype = new Sampler();
MultiJittered.prototype.generate_samples = function () {
    var n = parseInt(Math.sqrt(this.num_samples)),
        subcell_width = 1.0 / (this.num_samples),
        p, j, k;
    for (j = 0; j < this.num_samples * this.num_sets; j++) {
        this.samples.push(new Point2D(0,0));
    }
    // distribute points in the initial patterns
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                var rand = Math.rand_double(0, subcell_width);
                this.samples[j * n + k + p * this.num_samples].x = (k * n + j)
                    * subcell_width + rand;
                rand = Math.rand_double(0, subcell_width);
                this.samples[j * n + k + p * this.num_samples].y = (j * n + k)
                    * subcell_width + rand;
            }
        }
    }

    // shuffle x coordinates
    for (p = 0; p < this.num_sets; p++) {
        for (k = 0; k < n; k++) {
            for (j = 0; j < n; j++) {
                var i = Math.random_int(j, n - 1),
                    t = this.samples[k * n + j + p * this.num_samples].x;
                this.samples[k * n + j + p * this.num_samples].x = this.samples[k
                * n + i + p * this.num_samples].x;
                this.samples[k * n + i + p * this.num_samples].x = t;
            }
        }
    }

    // shuffle y coordinates
    for (p = 0; p < this.num_sets; p++) {
        for (k = 0; k < n; k++) {
            for (j = 0; j < n; j++) {
                var i = Math.random_int(j, n - 1),
                    t = this.samples[j * n + k + p * this.num_samples].y;
                this.samples[j * n + k + p * this.num_samples].y = this.samples[i
                * n + k + p * this.num_samples].y;
                this.samples[i * n + k + p * this.num_samples].y = t;
            }
        }
    }
};

function NRooks(num_samples, num_sets) {
    this.num_samples = num_samples;
    this.num_sets = num_sets;
    this.generate_samples();
}
NRooks.prototype = new Sampler();
NRooks.prototype.generate_samples = function () {
    var p, j,
        sp;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < this.num_samples; j++) {
            sp = new Point2D((j + Math.rand_double2()) / this.num_samples,
                (j + Math.rand_double2()) / this.num_samples);
            this.samples.push(sp);
        }
    }
    this.shuffle_x_coordinates();
    this.shuffle_y_coordinates();
};
NRooks.prototype.shuffle_x_coordinates = function () {
    var p, j;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < this.num_samples - 1; j++) {
            var target = Math.random_int2() % this.num_samples + p * this.num_samples,
                temp = this.samples[j + p * this.num_samples + 1].x;
            this.samples[j + p * this.num_samples + 1].x = this.samples[target].x;
            this.samples[target].x = temp;
        }
    }
};
NRooks.prototype.shuffle_y_coordinates = function () {
    var p, j;
    for (p = 0; p < this.num_sets; p++) {
        for (j = 0; j < this.num_samples - 1; j++) {
            var target = Math.random_int2() % this.num_samples + p * this.num_samples,
                temp = this.samples[j + p * this.num_samples + 1].y;
            this.samples[j + p * this.num_samples + 1].y = this.samples[target].y;
            this.samples[target].y = temp;
        }
    }
};