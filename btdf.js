function BTDF() {
    this.sampler = null;
}
BTDF.prototype = {
    f: function (sr, wi, wo) {
        return new Color(0, 0, 0);
    },
    // computes the direction of reflected rays
    sample_f: function (sr, wi, wo) {
        return new Color(0, 0, 0);
    },
    rho: function (sr, wo) {
        return new Color(0, 0, 0);
    },
    tir: function (sr) {
        return new Color(0, 0, 0);
    },
    set_eta : function (eta) {
        this.eta = eta;
    },
    set_eta_in : function (eta) {
        this.eta_in = eta;
    },
    set_eta_out : function (eta) {
        this.eta_out = eta;
    }
};
function PerfectTransmitter() {
    this.kd = null;
    this.cd = null;
    this.kt = 1;
}
PerfectTransmitter.prototype = new BTDF();
PerfectTransmitter.prototype.f = function (sr, wi, wo) {
    var L = BACKGROUND_COLOR;
    return L;
};
PerfectTransmitter.prototype.sample_f = function (sr, wo, wt, pdf) {
    var n = sr.normal,
        cosThetaI = n.dot(wo),
        eta = this.eta;
    if (cosThetaI < 0) {
        cosThetaI = -cosThetaI;
        n = n.negate();
        eta = 1 / eta;
    }
    //compute the angle of refraction
    var temp = 1 - (1 - cosThetaI * cosThetaI) / (eta * eta),
        cosTheta2 = Math.sqrt(temp);
    //use it to generate transmitted ray
    wt = (wo.negate().scale(1/eta).subtract(n.scale(cosTheta2 - cosThetaI / eta)));
    //figure out the amount of contribution of the transmission.
    var abs = Math.abs(sr.normal.dot(wt)),
        eta2 = eta * eta,
        kte = kt / eta2,
        mul = new Color(1,1,1).scale(kte),
        l = mul.scale(1/abs);
    return {l: l, wi: wt};
};
PerfectTransmitter.prototype.tir = function (sr) {
    var wo = sr.ray.direction.negate(),
        cosThetaI = sr.normal.dot(wo),
        eta = this.eta;
    if (cosThetaI < 0) {
        eta = 1 / eta;
    }
    return (1 - (1 - cosThetaI * cosThetaI) / (eta * eta)) < 0;
};

function FresnelTransmitter() {
    this.kd = null;
    this.cd = null;
    this.eta_in = 1;
    this.eta_out = 1
}
FresnelTransmitter.prototype = new BTDF();
FresnelTransmitter.prototype.sample_f = function (sr, wo, wt, pdf) {
    var n = sr.normal,
        cosThetaI = n.dot(wo),
        eta = this.eta_in / this.eta_out;
    if (cosThetaI < 0) {
        cosThetaI = -cosThetaI;
        n = n.negate();
        eta = 1 / eta;
    }
    //compute the angle of refraction
    var temp = 1 - (1 - cosThetaI * cosThetaI) / (eta * eta),
        cosTheta2 = Math.sqrt(temp);
    //use it to generate transmitted ray
    wt = (wo.negate().scale(1/eta).subtract(n.scale(cosTheta2 - cosThetaI / eta)));
    //figure out the amount of contribution of the transmission.
    var abs = Math.abs(sr.normal.dot(wt)),
        eta2 = eta * eta,
        kte = this.fresnel(sr) / eta2,
        mul = new Color(1,1,1).scale(kte),
        l = mul.scale(1/abs);
    return {l: l, wi: wt};
};
FresnelTransmitter.prototype.tir = function (sr) {
    var wo = sr.ray.direction.negate(),
        cosThetaI = sr.normal.dot(wo),
        eta,
        ndotd = sr.normal.negate().dot(sr.ray.direction);
    if (ndotd < 0) {
        eta = this.eta_out / this.eta_in;
    } else {
        eta = this.eta_in / this.eta_out;
    }
    return (1 - (1 - cosThetaI * cosThetaI) / (eta * eta)) < 0;
};
FresnelTransmitter.prototype.fresnel = function (sr) {
    var kr,
        normal = sr.normal,
        ndotd = normal.negate().dot(sr.ray.direction),
        eta;
    if (ndotd < 0) {
        normal = normal.negate();
        eta = this.eta_out / this.eta_in;
    } else {
        eta = this.eta_in / this.eta_out;
    }
    var cos_theta_i = normal.negate().dot(sr.ray.direction),
        cos_theta_t = Math.sqrt(1 - (1 - cos_theta_i*cos_theta_i) / (eta*eta)),
        r_parallel = (eta * cos_theta_i - cos_theta_t) / (eta * cos_theta_i + cos_theta_t),
        r_perpendicular = (cos_theta_i - eta*cos_theta_t) * (cos_theta_i + eta * cos_theta_t);
    kr = 0.5 * (r_parallel*r_parallel + r_perpendicular*r_perpendicular);
    return (1-kr);
};
FresnelTransmitter.prototype.set_eta_in = function (i) {
    this.eta_in = i;
};
FresnelTransmitter.prototype.set_eta_out = function (i) {
    this.eta_out = i;
};