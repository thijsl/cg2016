window.onload = function() {
    var runtime = document.getElementById("runtime"),
        canvas = document.getElementsByTagName("canvas")[0],
        errorSpan = document.getElementById("error"),
        ctx = canvas.getContext("2d"),
        startTime = new Date(),
        time = null,
        timeDiff = null,
        acc = "bvh",
        width = document.getElementById('width').value,
        height = document.getElementById('height').value,
        eye = {x:document.getElementById('eyeX').value, y: document.getElementById('eyeY').value, z: document.getElementById('eyeZ').value},
        lookat = {x:document.getElementById('lookatX').value, y: document.getElementById('lookatY').value, z: document.getElementById('lookatZ').value},
        up = {x:document.getElementById('upX').value, y: document.getElementById('upY').value, z: document.getElementById('upZ').value},
        camera = {eye: eye, lookat: lookat, up: up},
        light1 = {x: parseFloat(document.getElementById('origin1X').value), y: parseFloat(document.getElementById('origin1Y').value), z: parseFloat(document.getElementById('origin1Z').value), intensity: parseFloat(document.getElementById('intensity1').value)},
        light2 = {x: parseFloat(document.getElementById('origin2X').value), y: parseFloat(document.getElementById('origin2Y').value), z: parseFloat(document.getElementById('origin2Z').value), intensity: parseFloat(document.getElementById('intensity2').value)},
        light3 = {x: parseFloat(document.getElementById('origin3X').value), y: parseFloat(document.getElementById('origin3Y').value), z: parseFloat(document.getElementById('origin3Z').value), intensity: parseFloat(document.getElementById('intensity3').value)},
        lights = [light1, light2, light3],
        sampler,
        num_samples,
        num_sets,
        scene,
        blob = new Blob([document.querySelector('#worker1').textContent]),
        ww1 = new Worker(window.URL.createObjectURL(blob)),
        ww2 = new Worker(window.URL.createObjectURL(blob)),
        ww3 = new Worker(window.URL.createObjectURL(blob)),
        ww4 = new Worker(window.URL.createObjectURL(blob));
    var work = function (e) {
        var msg = e.data;
        if (msg.timeUpdate) {
            time = msg.timeUpdate;
            timeDiff = Math.abs((startTime.getTime() - time.getTime())/1000);
            runtime.innerHTML = timeDiff + "s";
        } else if (msg.drawPixel) {
            var r = Math.floor(msg.drawPixel.color.r*255),
                g = Math.floor(msg.drawPixel.color.g*255),
                b = Math.floor(msg.drawPixel.color.b*255);
            ctx.fillStyle = 'rgb(' + r + ',' + g  + ',' + b  +')';
            ctx.fillRect(msg.drawPixel.column, height - msg.drawPixel.row, 1, 1);
        } else if (msg.error) {
            errorSpan.innerHTML = msg.error;
        }
    };
    ww1.onmessage = work;
    ww2.onmessage = work;
    ww3.onmessage = work;
    ww4.onmessage = work;

    document.getElementById('start').addEventListener('click', start, false);

    function start() {
        var objects = getSelectedObjects();
        acc =  getSelectValues(document.getElementById('acc'))[0] || "bvh",
            width = document.getElementById('width').value;
        height = document.getElementById('height').value;
        eye = {x:parseFloat(document.getElementById('eyeX').value), y: parseFloat(document.getElementById('eyeY').value), z: parseFloat(document.getElementById('eyeZ').value)};
        lookat = {x:parseFloat(document.getElementById('lookatX').value), y: parseFloat(document.getElementById('lookatY').value), z: parseFloat(document.getElementById('lookatZ').value)};
        up = {x:parseFloat(document.getElementById('upX').value), y: parseFloat(document.getElementById('upY').value), z: parseFloat(document.getElementById('upZ').value)};
        camera = {eye: eye, lookat: lookat, up: up};
        light1 = {x: parseFloat(document.getElementById('origin1X').value), y: parseFloat(document.getElementById('origin1Y').value), z: parseFloat(document.getElementById('origin1Z').value), intensity: parseFloat(document.getElementById('intensity1').value)};
        light2 = {x: parseFloat(document.getElementById('origin2X').value), y: parseFloat(document.getElementById('origin2Y').value), z: parseFloat(document.getElementById('origin2Z').value), intensity: parseFloat(document.getElementById('intensity2').value)};
        light3 = {x: parseFloat(document.getElementById('origin3X').value), y: parseFloat(document.getElementById('origin3Y').value), z: parseFloat(document.getElementById('origin3Z').value), intensity: parseFloat(document.getElementById('intensity3').value)};
        lights = [light1, light2, light3],
            sampler = document.getElementById("sampler").value;
        num_samples = parseInt(document.getElementById("num_samples").value);
        num_sets = parseInt(document.getElementById("num_sets").value);
        scene = document.getElementById("scene").value;
        canvas = document.getElementsByTagName("canvas")[0];
        canvas.width = width;
        canvas.height = height;
        startTime = new Date();
        runtime.innerHTML = "<strong>Raytracing started!</strong>";
			// ww1.postMessage({url: document.location.href, dim: {height: height, width: width, rStart: 0, rEnd: width, cStart: 0, cEnd: height},
			// 	objects: objects, camera: camera, lights: lights, acc: acc, sampler: sampler, num_samples: num_samples, num_sets: num_sets, scene: scene});
        ww1.postMessage({url: document.location.href, dim: {height: height, width: width, rStart: 0, rEnd: width/2, cStart: 0, cEnd: height/2},
            objects: objects, camera: camera, lights: lights, acc: acc, sampler: sampler, num_samples: num_samples, num_sets: num_sets, scene: scene});
        ww2.postMessage({url: document.location.href, dim: {height: height, width: width, rStart: width/2, rEnd: width, cStart: 0, cEnd: height/2},
            objects: objects, camera: camera, lights: lights, acc: acc, sampler: sampler, num_samples: num_samples, num_sets: num_sets, scene: scene});
        ww3.postMessage({url: document.location.href, dim: {height: height, width: width, rStart: 0, rEnd: width/2, cStart: height/2, cEnd: height},
            objects: objects, camera: camera, lights: lights, acc: acc, sampler: sampler, num_samples: num_samples, num_sets: num_sets, scene: scene});
        ww4.postMessage({url: document.location.href, dim: {height: height, width: width, rStart: width/2, rEnd: width, cStart: height/2, cEnd: height},
            objects: objects, camera: camera, lights: lights, acc: acc, sampler: sampler, num_samples: num_samples, num_sets: num_sets, scene: scene});
    }
};

function getSelectedObjects() {
    var objects = document.getElementById('objects');
    objects = getSelectValues(objects);
    return objects;
}

function getSelectValues(select) {
    var result = [],
        options = select && select.options,
        opt,
        i = 0,
        iLen = options.length;
    for (i; i<iLen; i++) {
        opt = options[i];
        if (opt.selected) {
            result.push(opt.value || opt.text);
        }
    }
    return result;
}