function BRDF() {
    this.sampler = null;
    this.normal = new Vector(0, 0, 0);
}
BRDF.prototype = {
    f: function (sr, wi, wo) {
        return new Color(0, 0, 0);
    },
    // computes the direction of reflected rays
    sample_f: function (sr, wi, wo) {
        return new Color(0, 0, 0);
    },
    rho: function (sr, wo) {
        return new Color(0, 0, 0);
    }
};
function Lambertian() {
    this.kd = 0;
    this.cd = new Color(0,0,0);
}
Lambertian.prototype = new BRDF();
Lambertian.prototype.f = function (sr, wi, wo) {
    var rho = this.rho(sr, wo),
        f = rho.scale(1 / Math.PI);
    assert(f instanceof Color, "Lambertian.f isn't a color.");
    return f;
};
Lambertian.prototype.rho = function (sr, wo) {
    return (this.cd.scale(this.kd))
};

function CookTorrance(eta_in, eta_out, p0, p1) {
    this.ks = 0.05; // [0,1]
    this.f0 = p0;
    this.m = p1;
    this.fresnel_reflector = new FresnelReflector(eta_in, eta_out);
}
CookTorrance.prototype = new BRDF();
// wi = outgoing vector (view), wo = incoming vector (light)
CookTorrance.prototype.f = function (sr, wi, wo) {
    wi = wi.normalize();
    wo = wo.normalize();
    sr.normal = sr.normal.normalize();
    var L = BACKGROUND_COLOR,
        m = this.m,
        f = this.f0,
        ndotwi = sr.normal.dot(wi),
        ndotwo = sr.normal.dot(wi),
        h = wi.add(wo).normalize(),
        ndoth = sr.normal.dot(h),
        widoth = wi.dot(h),
        geom = Math.min(1, Math.min((2*ndotwi*ndoth)/widoth), (2*ndotwo*ndoth)/widoth),
        dCoeff = 1 / ((m*m)*Math.pow(Math.cos(ndoth),4)),
        dPow = -(Math.pow((Math.tan(ndoth) / m), 2)),
        d = dCoeff * Math.exp(dPow),
        // dCoeff = 1 / (Math.PI * m*m * Math.pow(ndoth, 4)),
        // dPow = ((Math.pow(ndoth, 2)-1) /
        //     (m*m*Math.pow(ndoth,2))),
        // d = dCoeff * Math.exp(dPow),
        fresnel = this.fresnel_reflector.fresnel2(this.f0, widoth);
    if (d === 0)
        d = kEpsilon;
    var ct = ((d * geom* fresnel) / (ndotwi * ndotwo)) ;

    if (ct >= 0) {
        ct;
        L = this.rho(sr).scale(ct);
    }
    assert(L instanceof Color, "CookTorrance.f isn't a color.", L);
    return L;
};
CookTorrance.prototype.rho = function (sr, wo) {
    return this.cs;// new Color(0.0291,0.0193,0.0118);
};
CookTorrance.prototype.sample_f = function (sr, wo, wi, pdf) {
    var ndotwo = sr.normal.dot(wo),
        r = wo.negate().add(sr.normal.scale(2*ndotwo)),
        w = r,
        u = new Vector(0.00424, 1, 0.00764).cross(w).normalize(),
        v = u.cross(w),
        sp = this.sampler.sample_hemisphere(),
        wi = u.scale(sp.x).add(v.scale(sp.y).add(w.scale(sp.z)));
    if (sr.normal.dot(wi) < 0) {
        wi = u.scale(-sp.x).add(v.scale(-sp.y).add(w.scale(sp.z)));
    }
    var phong_lobe = Math.pow(r.dot(w), this.exp);
    pdf = phong_lobe * sr.normal.dot(wi);
    var l = this.cs.scale((this.ks*phong_lobe));
    return {l: color, pdf: pdf, wi: wi};
};

function GlossySpecular() {
    this.cs = new Color(0.2, 0.2, 0.2);
    this.ks = 0.05; // [0,1]
    this.exp = 2;
}
GlossySpecular.prototype = new BRDF();
GlossySpecular.prototype.f = function (sr, wi, wo) {
    var L = BACKGROUND_COLOR,
        ndotwi = sr.normal.dot(wi),
        r = wi.negate().add(sr.normal.scale(2 * ndotwi)),
        rdotwo = r.dot(wo);
    if (rdotwo > 0.0) {
        L = this.cs.scale(this.ks * Math.pow(rdotwo, this.exp));
    }
    assert(L instanceof Color, "GlossySpecular.f isn't a color.");
    return L;
};
GlossySpecular.prototype.rho = function (sr, wo) {
    return (this.cd.scale(this.kd))
};
GlossySpecular.prototype.sample_f = function (sr, wo, wi, pdf) {
    var ndotwo = sr.normal.dot(wo),
        r = wo.negate().add(sr.normal.scale(2*ndotwo)),
        w = r,
        u = new Vector(0.00424, 1, 0.00764).cross(w).normalize(),
        v = u.cross(w),
        sp = this.sampler.sample_hemisphere(),
        wi = u.scale(sp.x).add(v.scale(sp.y).add(w.scale(sp.z)));
    if (sr.normal.dot(wi) < 0) {
        wi = u.scale(-sp.x).add(v.scale(-sp.y).add(w.scale(sp.z)));
    }
    var phong_lobe = Math.pow(r.dot(w), this.exp);
    pdf = phong_lobe * sr.normal.dot(wi);
    var l = this.cs.scale((this.ks*phong_lobe));
    return {l: l, pdf: pdf, wi: wi};
};

function SV_Lambertian() {
    this.kd = 0;
    this.cd = new Texture();
}
SV_Lambertian.prototype = new BRDF();
SV_Lambertian.prototype.f = function (sr, wi, wo) {
    var rho = this.rho(sr, wo),
        f = rho.scale(1 / Math.PI);
    assert(f instanceof Color, "f isn't a color.");
    return f;
};
SV_Lambertian.prototype.rho = function (sr, wo) {
    return (this.cd.get_color(sr).scale(this.kd))
};

function FresnelReflector(eta_in, eta_out) {
    this.eta_in = eta_in || 1;    // index of refraction interior
    this.eta_out = eta_out || 1;   // index of refraction exterior
}
FresnelReflector.prototype = new BRDF();
FresnelReflector.prototype.fresnel = function (sr) {
    var kr,
        normal = sr.normal,
        ndotd = normal.negate().dot(sr.ray.direction),
        eta;
    if (ndotd < 0) {
        normal = normal.negate();
        eta = this.eta_out / this.eta_in;
    } else {
        eta = this.eta_in / this.eta_out;
    }
    var cos_theta_i = normal.negate().dot(sr.ray.direction),
        cos_theta_t = Math.sqrt(1 - (1 - cos_theta_i*cos_theta_i) / (eta*eta)),
        r_parallel = (eta * cos_theta_i - cos_theta_t) / (eta * cos_theta_i + cos_theta_t),
        r_perpendicular = (cos_theta_i - eta*cos_theta_t) * (cos_theta_i + eta * cos_theta_t);
    kr = 0.5 * (r_parallel*r_parallel + r_perpendicular*r_perpendicular);
    return kr;
};
FresnelReflector.prototype.fresnel2 = function(eta, theta) {
    var cos_theta_i = Math.cos(theta),
        cos_theta_t = Math.sqrt(Math.abs(1 - (1 - cos_theta_i*cos_theta_i) / (eta*eta))),
        r_parallel = (eta * cos_theta_i - cos_theta_t) / (eta * cos_theta_i + cos_theta_t),
        r_perpendicular = (cos_theta_i - eta*cos_theta_t) * (cos_theta_i + eta * cos_theta_t);
    return 0.5 * (r_parallel*r_parallel + r_perpendicular*r_perpendicular);
};
FresnelReflector.prototype.sample_f = function (sr, wo, wr) {
    var ndotwo = sr.normal.dot(wo),
        wr = wo.negate().add(sr.normal.scale(ndotwo*2)),
        l = new Color(1,1,1).scale(this.fresnel(sr) * Math.abs(sr.normal.dot(wr)));
    return {l : l, wi: wr}; // return color and reflection direction
};
FresnelReflector.prototype.set_eta_in = function (i) {
    this.eta_in = i;
};
FresnelReflector.prototype.set_eta_out = function (i) {
    this.eta_out = i;
};