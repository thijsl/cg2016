function Vector(x,y,z) {
	this.x = x;
	this.y = y;
	this.z = z;
}
Vector.prototype =  {
	add : function(other) {
		return new Vector(this.x+other.x,this.y+other.y,this.z+other.z);
	},
	subtract : function(other) {
		return new Vector(this.x-other.x,this.y-other.y,this.z-other.z);
	},
	scale : function(other) {
		return new Vector(this.x*other,this.y*other,this.z*other);
	},
	divide : function(other) {
		return new Vector(this.x/other,this.y/other,this.z/other);
	},
	euclidean_length : function() {
		return Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));
	},
	squared_length : function() {
		return this.x*this.x + this.y*this.y + this.z*this.z;
	},
	normalize : function() {
		return this.scale(1/this.euclidean_length());
	},
	dot : function(other) {
		return (this.x*other.x + this.y*other.y + this.z*other.z);
	},
	dotVector : function(other) {
		return (this.x*other.x + this.y*other.y + this.z*other.z);
	},
	cross : function(other) { // cross is represented by ^
		return new Vector(this.y*other.z-this.z*other.y,this.z*other.x-this.x*other.z,this.x*other.y-this.y*other.x);
	},
	hat : function() { // hat or unit vector
		return this.normalize();
	},
	multiply : function(other) {
		return new Vector(this.x*other.x,this.y*other.y,this.z*other.z);
	},
	reflect : function(normal) {
		return this.add(normal.scale(2*this.dot(normal)));
	},
	negate : function() {
		return this.scale(-1);
	},
	toNormal : function() {
		return new Normal(this.x, this.y, this.z);
	}
};
Vector.getVector = function(other) {
	return new Vector(other.x, other.y, other.c)
};

function Point(x,y,z) {
	this.x = x;
	this.y = y;
	this.z = z;
}
Point.prototype = {
	add : function(vector) {
		return new Point(this.x+vector.x,this.y+vector.y,this.z+vector.z);
	},
	subtractVector : function(vector) {
		return new Point(this.x-vector.x,this.y-vector.y,this.z-vector.z);
	},
	subtract : function(other) {
		return new Vector(this.x-other.x,this.y-other.y,this.z-other.z);
	},
	divide : function(other) {
		return new Vector(this.x/other,this.y/other,this.z/other);
	},
	scale : function(value) {
		return new Point(this.x*value,this.y*value,this.z*value);
	},
	cross : function(other) {
		return  new Vector(this.y*other.z-this.z*other.y,this.z*other.x-this.x*other.z,this.x*other.y-this.y*other.x);
	},
	euclidean_length : function(other) {
		return Math.sqrt((this.x-other.x)*(this.x-other.x)+(this.y-other.y)*(this.y-other.y)+(this.z-other.z)*(this.z-other.z));
	},
	squared_length : function() {
		return this.x*this.x + this.y*this.y + this.z*this.z;
	},
	normalize : function() {
		return Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));
	},
	negate : function() {
		return this.scale(-1);
	},
	toVector: function() {
		return new Vector(this.x, this.y, this.z);
	},
	d_squared : function(other) {
		return (this.x - other.x) * (this.x - other.x) + (this.y - other.y) *
			(this.y - other.y) + (this.z - other.z) * (this.z - other.z);
	}
};

function Normal(x, y, z) {
	this.x = x;
	this.y = y;
	this.z = z;
}

Normal.prototype = new Vector();

Normal.prototype.normalize = function() {
	var euclideanLength = Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));
	return new Normal(this.x/euclideanLength, this.y/euclideanLength, this.z/euclideanLength);
};

function Point2D(x,y) {
	this.x = x;
	this.y = y;
}
Point2D.prototype = {
	toString : function() {
		return [this.x,this.y];
	},
	subtract : function(other) {
		return new Point2D(this.x-other.x,this.y-other.y);
	}
};
Point2D.getPoint2D = function(other) {
	return new Point2D(other.x, other.y)
};

function Color(r,g,b) {
	this.r = r;
	this.g = g;
	this.b = b;
}
Color.prototype = {
	add : function(other) {
		return new Color(this.r+other.r,this.g+other.g,this.b+other.b);
	},
	subtract : function(other) {
		return new Color(this.r-other.r,this.g-other.g,this.b-other.b);
	},
	divide : function(other) {
		return new Color(this.r/other,this.g/other,this.b/other);
	},
	scale : function(other) {
		return new Color(this.r*other,this.g*other,this.b*other);
	},
	cross : function(other) { // cross is represented by ^
		return new Color(this.g*other.b-this.b*other.g,this.b*other.r-this.r*other.b,this.r*other.g-this.g*other.r);
	},
	multiply : function(other) {
		return new Color(this.r*other.r,this.g*other.g,this.b*other.b);
	},
	pow : function(exp) {
		return new Color(Math.pow(this.r, exp), Math.pow(this.g, exp), Math.pow(this.b, exp));
	},
	negate : function() {
		return this.scale(-1);
	},
};
var BACKGROUND_COLOR = new Color(0,0,0);

function Transformation(matrix, inverse) {
	this.matrix = matrix;
	this.inverse = inverse;
};
Transformation.prototype.translate = function(x,y,z) {
	var transformation = new Matrix([
		[1,	0,	0,	x],
		[0,	1,	0,	y],
		[0,	0,	1,	z],
		[0,	0,	0,	1]
		]),
		inverse = new Matrix([
		[1,	0,	0,	-x],
		[0,	1,	0,	-y],
		[0,	0,	1,	-z],
		[0,	0,	0,	1]
		]);
	return new Transformation(transformation, inverse);
};
Transformation.prototype.scale = function(x, y, z) {
	var transformation = new Matrix([
		[x,	0,	0,	0],
		[0,	y,	0,	0],
		[0,	0,	z,	0],
		[0,	0,	0,	1]
		]),
		inverse = new Matrix([
		[1/x,	0,		0,		0],
		[0,		1/y,	0,		0],
		[0,		0,		1/z,	0],
		[0,		0,		0,		1]
		]);
	return new Transformation(transformation, inverse);
};
Transformation.prototype.append = function(transformation) {
	var append = new Transformation(
		this.matrix.multiply(transformation.matrix),
		transformation.inverse.multiply(this.inverse)
	);

	return append;
};
Transformation.prototype.transformInverse = function(object) {
	if (object instanceof Ray) {
		var point = this.transformInverse(object.origin);
		var direction = this.transformInverse(object.direction);
		return new Ray(point, direction);
	} else if (object instanceof Point) {
		return this.inverse.transform(object);
	} else if (object instanceof Normal) {
		return this.inverse.transform(object);
	} else if (object instanceof Vector) {
		return this.inverse.transform(object);
	}
};

function Matrix(matrix) {
	if (arguments.length > 0) {
		this.matrix = matrix;
	} else {
		this.matrix = [[],[],[],[]];
	}
};
Matrix.prototype.multiply = function(matrix) {
	if (matrix instanceof Matrix) {
		for (var row = 0; row < 4; row++) {
			for (var column = 0; column < 4; column++) {
				var value = 0;
				for (var k = 0; k < 4; ++k) {
					value += this.matrix[row][k] * matrix.matrix[k][column];
				}
				this.matrix[row][column] = value;
			}
		}
		return this;
	}
	else if (matrix.length === 4) {
		var result = [0, 0, 0, 0];
		for (var i = 0; i < 4; ++i)
		for (var k = 0; k < 4; ++k)
		result[i] += this.matrix[i][k] * matrix[k];
		return result;
	}
};
Matrix.prototype.transform = function(object) {
	if (object instanceof Point) {
		var homogenous = [object.x, object.y, object.z, 1.0];
		var transformed = this.multiply(homogenous);
		return new Point(transformed[0], transformed[1], transformed[2],
			transformed[3]);
	} else if (object instanceof Vector || object instanceof Normal) {
		var v = [0,0,0],
			homogenous = [object.x, object.y, object.z, 1.0];
		for (var row = 0; row < 3; row++) {
			for (var column = 0; column < 3; column++) {
				v[row] += this.matrix[row][column] * homogenous[column];
			}
		}
		if (object instanceof Normal) {
			return new Normal(v[0], v[1], v[2]);
		} else if (object instanceof Vector) {
			return new Vector(v[0], v[1], v[2]);
		}
	}
};

function ComplexNumber(real, imaginary) {
	this.real = real;
	this.imaginary = imaginary;
}
ComplexNumber.prototype.multiply = function(other) {

}
ComplexNumber.prototype.negate = function(){

}
ComplexNumber.prototype.divide = function(other) {

}
ComplexNumber.prototype.subtract = function(other) {

}

Math.rand_double = function(l, h) {
	var randd = this.rand_double2();
	return randd * (h - l) + l;
};
Math.rand_double2 = function() {
	return this.random_int(0, Number.MAX_VALUE) / Number.MAX_VALUE;
};
Math.random_int = function(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};
Math.random_int2 = function() {
	return this.random_int(0, Number.MAX_VALUE);
};
Math.solveQuartic = function(coeffs) {
	var roots = 0,
		a = new ComplexNumber(coeffs[0], 0),
		b = new ComplexNumber(coeffs[1], 0),
		c = new ComplexNumber(coeffs[2], 0),
		d = new ComplexNumber(coeffs[3], 0),
		e = new ComplexNumber(coeffs[4], 0);
	// if (delt().real.doubleValue() > 0) {
	// 	if (P().real.doubleValue() > 0 || D().real.doubleValue() > 0) {
	// 		return 0;
	// 	}
	// }
	// var nbo4a = b.negate().div(a.mult(4));
	// ComplexNumber S = S();
	// ComplexNumber n4s2 = S.mult(S).mult(-4);
	// ComplexNumber twop = p().mult(2);
	// ComplexNumber qos = q().div(S);
	// ComplexNumber r12SqrtInner = n4s2.sub(twop).add(qos);
	// ComplexNumber r34SqrtInner = n4s2.sub(twop).sub(qos);
	// ComplexNumber root1 = nbo4a.sub(S).add(r12SqrtInner.sqrt().mult(0.5));
	// if (!root1.isImaginary()) {
	// 	ret[roots] = root1.real.doubleValue();
	// 	roots++;
	// }
	// ComplexNumber root2 = nbo4a.sub(S).sub(r12SqrtInner.sqrt().mult(0.5));
	// if (!root2.isImaginary()) {
	// 	ret[roots] = root2.real.doubleValue();
	// 	roots++;
	// }
	// ComplexNumber root3 = nbo4a.add(S).add(r34SqrtInner.sqrt().mult(0.5));
	// if (!root3.isImaginary()) {
	// 	ret[roots] = root3.real.doubleValue();
	// 	roots++;
	// }
	// ComplexNumber root4 = nbo4a.add(S).sub(r34SqrtInner.sqrt().mult(0.5));
	// if (!root4.isImaginary()) {
	// 	ret[roots] = root4.real.doubleValue();
	// 	roots++;
	// }

	return roots;
}