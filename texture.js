function Texture() {
}
Texture.prototype.color = new Color(0,0,0);
Texture.prototype.get_color = function(sr) {
    return this.color;
};
Texture.prototype.set_color = function(color) {
    this.color = color;
};

function ConstantColor(color) {
    this.set_color(color);
}
ConstantColor.prototype = new Texture();

function ImageTexture(hres, vres, image_ptr, mapping_ptr) {
    this.hres = hres;
    this.vres = vres;
    this.image_ptr = image_ptr;
    this.mapping_ptr = mapping_ptr;
}
ImageTexture.prototype = new Texture();
ImageTexture.prototype.get_color = function(sr) {
    var row, column, data;

    if (this.mapping_ptr) {
        data = this.mapping_ptr.get_texel_coordinates(sr.local_hit_point, this.hres, this.vres);
        row = data.row;
        column = data.column;
    } else {
        row = parseInt(sr.v * (vres - 1));
        column = parseInt(sr.u * (hres - 1));
    }
    
    return (this.image_get_color(row, column));
};
ImageTexture.prototype.image_get_color = function(row, column) {
        var pixels = this.image_ptr.pixels,
            index = column + this.hres * (this.vres - row - 1),
            n = pixels.length;
        if (index < n && index >= 0) {
            var color = pixels[index];
            return new Color(color.r, color.g, color.b);
        } else {
            return new Color(0, 1, 0);
        }
}

function Mapping() {

}
Mapping.prototype.get_pixel_coordinates = function(local_hit_point, hres, vres, row, column) {

};

function SphericalMap() {

}
SphericalMap.prototype = new Mapping();
SphericalMap.prototype.get_texel_coordinates = function(local_hit_point, hres, vres) {
    var theta = Math.acos(local_hit_point.y),
        phi = Math.atan2(local_hit_point.x, local_hit_point.y);
    if (phi < 0.0) {
        phi += (Math.PI*2);
    }
    var u = phi * (1/(Math.PI*2)),
        v = 1 - (theta*(1/(Math.PI*2)));

    var column = parseInt((hres -1) * u), // column is across
        row = parseInt((vres - 1) * v); // row is up
    return {column: column, row: row};
};

function LightProbe(){

}
LightProbe.prototype = new Mapping();
LightProbe.prototype.light_probe = true;
LightProbe.prototype.get_texel_coordinates = function(hit_point, hres, vres) {
    var x = hit_point.x,
        y = hit_point.y,
        z = hit_point.z,
        d = Math.sqrt(x*x + y*y),
        sin_beta = y /d,
        cos_beta = x/d,
        alpha = (this.light_probe) ? Math.acos(z) : Math.acos(-z);

    var r = alpha * (1/(Math.PI*2)),
        u = (1.0 + (r*cos_beta)) * 0.5,
        v = (1.0 + (r*sin_beta)) * 0.5;

    var column = parseInt((hres -1) * u), // column is across
        row = parseInt((vres - 1) * v); // row is up
    return {column: column, row: row};
};

function NormalMap(){

}
NormalMap.prototype = new Mapping();
NormalMap.prototype.compute_tangent = function(vertices, uvs, normals) {
    var tangents = [], bitangents = [];
    for (var i = 0; i < vertices.length; i+=3) {
        var v0 = Vector.getVector(vertices[i+0]),
            v1 = Vector.getVector(vertices[i+1]),
            v2 = Vector.getVector(vertices[i+2]),
            uv0 = Point2D.getPoint2D(uvs[i+0]),
            uv1 = Point2D.getPoint2D(uvs[i+1]),
            uv2 = Point2D.getPoint2D(uvs[i+2]),
            deltaPos1 = v1.subtract(v0),
            deltaPos2 = v2.subtract(v0),
            deltaUv1 = uv1.subtract(uv0),
            deltaUv2 = uv2.subtract(uv0),

            r = 1 / ( deltaUv1.x * deltaUv2.y - deltaUv1.y * deltaUv2.x ),
            tangent = (deltaPos1.scale(deltaUv2.y).subtract(deltaPos2.scale(deltaUv1.y))).scale(r),
            bitangent = (deltaPos2.scale(deltaUv1.y).subtract(deltaPos1.scale(deltaUv2.x))).scale(r);

        tangents.push(tangent, tangent, tangent);
        bitangents.push(bitangent, bitangent, bitangent);
    }
    return {tangents : tangents, bitangents : bitangents};
};
NormalMap.prototype.get_texel_coordinates = function(hit_point, hres, vres) {
    var x = hit_point.x,
        y = hit_point.y,
        z = hit_point.z,
        d = Math.sqrt(x*x + y*y),
        sin_beta = y /d,
        cos_beta = x/d,
        alpha = (this.light_probe) ? Math.acos(z) : Math.acos(-z);

    var r = alpha * (1/(Math.PI*2)),
        u = (1.0 + (r*cos_beta)) * 0.5,
        v = (1.0 + (r*sin_beta)) * 0.5;

    var column = parseInt((hres -1) * u), // column is across
        row = parseInt((vres - 1) * v); // row is up
    return {column: column, row: row};
};
